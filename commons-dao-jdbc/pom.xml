<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.kopek.commons</groupId>
        <artifactId>commons-dao</artifactId>
        <version>1.0.1</version>
    </parent>
    <artifactId>commons-dao-jdbc</artifactId>
    <packaging>jar</packaging>

    <!-- /// Informations générales /// -->
    <name>Commons DAO Jdbc</name>
    <description>Module concernant les DAO's utilisant les connexions JDBC</description>

    <!-- /// Liste des profiles /// -->
    <profiles>

        <!-- Profiles des tests fonctionnels -->
        <profile>
            <id>with-func-test</id>
            <activation>
                <property>
                    <name>with-func-test</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <testResources>
                    <testResource>
                        <directory>src/test/resources</directory>
                        <filtering>true</filtering>
                    </testResource>
                    <testResource>
                        <directory>src/test-it/resources</directory>
                        <filtering>true</filtering>
                    </testResource>
                </testResources>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>build-helper-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>add-test-source</id>
                                <phase>generate-test-sources</phase>
                                <goals>
                                    <goal>add-test-source</goal>
                                </goals>
                                <configuration>
                                    <sources>
                                        <source>src/test-it/java</source>
                                    </sources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <includes>
                                <include>${integration.test.pattern}</include>
                                <include>${unit.test.pattern}</include>
                            </includes>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.liquibase</groupId>
                        <artifactId>liquibase-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>init-test-bdd</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>update</goal>
                                </goals>
                                <configuration>
                                    <changeLogFile>bdd/changelog.xml</changeLogFile>
                                    <url>${db.url}</url>
                                    <username>${db.username}</username>
                                    <password>${db.password}</password>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>with-docker-func-test</id>
            <activation>
                <property>
                    <name>with-docker-func-test</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <testResources>
                    <testResource>
                        <directory>src/test/resources</directory>
                        <filtering>true</filtering>
                    </testResource>
                    <testResource>
                        <directory>src/test-it/resources</directory>
                        <filtering>true</filtering>
                    </testResource>
                </testResources>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>build-helper-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>add-test-source</id>
                                <phase>generate-test-sources</phase>
                                <goals>
                                    <goal>add-test-source</goal>
                                </goals>
                                <configuration>
                                    <sources>
                                        <source>src/test-it/java</source>
                                    </sources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <configuration>
                            <includes>
                                <include>${integration.test.pattern}</include>
                                <include>${unit.test.pattern}</include>
                            </includes>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>start-test-container</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <workingDirectory>target/test-classes</workingDirectory>
                                    <executable>docker</executable>
                                    <arguments>
                                        <argument>compose</argument>
                                        <argument>up</argument>
                                        <argument>-d</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                            <execution>
                                <id>stop-test-container</id>
                                <phase>prepare-package</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <workingDirectory>target/test-classes</workingDirectory>
                                    <executable>docker</executable>
                                    <arguments>
                                        <argument>compose</argument>
                                        <argument>stop</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.liquibase</groupId>
                        <artifactId>liquibase-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>init-test-bdd</id>
                                <phase>process-test-classes</phase>
                                <goals>
                                    <goal>update</goal>
                                </goals>
                                <configuration>
                                    <changeLogFile>bdd/changelog.xml</changeLogFile>
                                    <url>${db.url}</url>
                                    <username>${db.username}</username>
                                    <password>${db.password}</password>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

    </profiles>

    <!-- /// Liste dépendances /// -->
    <dependencies>

        <!-- // Liste des modules // -->

        <dependency>
            <groupId>org.kopek.commons</groupId>
            <artifactId>commons-dao-base</artifactId>
        </dependency>

        <!-- // Librairies générales // -->

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>

        <!-- // Librairies de tests // -->

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>

    </dependencies>

</project>