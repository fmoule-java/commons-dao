package org.commons.dao.jdbc.query.condition;

import org.apache.commons.lang3.math.NumberUtils;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.query.general.ToSQL;
import org.commons.dao.jdbc.utils.SQLUtils;

import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.EQUAL;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.NOT_EQUAL;

/**
 * <p>
 * Condition SQL testant l'égalité d'un champs d'une table avec
 * une valeur donnée. <br />
 * </p>
 *
 * @author Frédéric Moulé
 */
public class SQLFieldOperatorCondition extends AbstractSQLCondition implements Comparable<SQLFieldOperatorCondition> {
    private static final String SQL_NULL_VALUE = "NULL";
    private final SQLField sqlField;
    private final SQLOperator sqlOperator;
    private final Object sqlValue;

    /// // Constructeur(s)

    private SQLFieldOperatorCondition(final SQLField sqlField, final SQLOperator sqlOperator, final Object sqlValue) {
        this.sqlField = requireNonNull(sqlField, "Le champs SQL ne doit pas être nul");
        this.sqlOperator = requireNonNull(sqlOperator, "L'opérateur SQL ne doit pas être nul");
        this.sqlValue = sqlValue;
    }

    @Override
    public SQLCondition negate() {
        return new SQLFieldOperatorCondition(this.sqlField, this.sqlOperator.negate(), this.sqlValue);
    }

    /// // Méthode(s) de la classe Object

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SQLFieldOperatorCondition sqlFieldOperatorCondition)) {
            return false;
        }
        boolean areEquals = Objects.equals(this.sqlField, sqlFieldOperatorCondition.sqlField);
        areEquals &= Objects.equals(this.sqlOperator, sqlFieldOperatorCondition.sqlOperator);
        areEquals &= Objects.equals(this.sqlValue, sqlFieldOperatorCondition.sqlValue);
        return areEquals;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.sqlField, this.sqlOperator, this.sqlValue);
    }

    /// // Méthode(s) de l'interface ToSQL

    /**
     * Méthode permettant de générer le code SQL
     * correspondant à l'instance courante
     *
     * @return code SQL
     */
    @Override
    public String toSQL() {
        final StringBuilder builder = new StringBuilder();
        if (sqlField.hasPrefix()) {
            builder.append(sqlField.getPrefix());
            builder.append(".");
        }
        builder.append(sqlField.getName());
        if (this.getValue() == null) {
            builder.append(" IS ");
        } else {
            builder.append(this.sqlOperator.toSQL());
        }
        builder.append(this.getSQLValue());
        return builder.toString();
    }

    @Override
    public int compareTo(final SQLFieldOperatorCondition sqlFieldOperatorCondition) {
        if (sqlFieldOperatorCondition == null) {
            return 1;
        }
        return this.sqlField.compareTo(sqlFieldOperatorCondition.sqlField);
    }

    /// // Méthode(s) statique(s)

    /**
     * Méthode statique permettant de construire une condition d'égalité.
     *
     * @param fieldPrefix : préfix du champs
     * @param fieldName   : nom du champs
     * @param fieldValue  : valeur
     * @return instance de SQLFieldEquality
     */
    public static SQLFieldOperatorCondition withFieldEqualsTo(final String fieldPrefix, final String fieldName, final Object fieldValue) {
        return withFieldEqualsTo(new SQLField(fieldPrefix, fieldName), fieldValue);
    }

    public static SQLFieldOperatorCondition withFieldEqualsTo(final SQLField sqlField, final Object value) {
        return new SQLFieldOperatorCondition(sqlField, EQUAL, value);
    }

    public static SQLFieldOperatorCondition withFieldNotEqualsTo(final String fieldPrefix, final String fieldName, final Object fieldValue) {
        return new SQLFieldOperatorCondition(new SQLField(fieldPrefix, fieldName), NOT_EQUAL, fieldValue);
    }

    public static SQLCondition withFieldOperator(SQLField sqlField, SQLOperator sqlOperateur, Object value) {
        return new SQLFieldOperatorCondition(sqlField, sqlOperateur, value);
    }

    /// // Getters & Setters

    public Object getValue() {
        return this.sqlValue;
    }

    public String getSQLValue() {
        final Object fieldValue = this.getValue();
        if (fieldValue instanceof Number numberValue) {
            return numberValue.toString();
        } else if (fieldValue instanceof SQLField theSqlField) {
            return theSqlField.toSQL();
        } else if (fieldValue != null) {
            return "\'" + SQLUtils.escapeSpecialCharacters(fieldValue.toString()) + "\'";
        } else {
            return SQL_NULL_VALUE;
        }
    }

    public String getFieldName() {
        return (this.sqlField == null ? null : this.sqlField.getName());
    }

    /// // Classe(s) interne(s)

    @FunctionalInterface
    public interface ComparisonAlgo {

        boolean compare(final Object obj1, final Object obj2);
    }

    /**
     * Enumération représentant les opérateurs
     * utilisables dans les requêtes SQL. <br />
     */
    public enum SQLOperator implements ToSQL {
        EQUAL("="),
        NOT_EQUAL("!="),
        GREATER_OR_EQUAL(">="),
        SMALLER_OR_EQUAL("<="),
        GREATER(">"),
        SMALLER("<");

        private final String sqlSymbol;

        /// // Constructeur(s)

        SQLOperator(final String sqlSymbol) {
            this.sqlSymbol = sqlSymbol;
        }

        /// // Méthode(s)

        @Override
        public String toSQL() {
            return " " + this.sqlSymbol + " ";
        }

        public SQLOperator negate() {
            if (this == EQUAL) {
                return NOT_EQUAL;
            } else if (this == NOT_EQUAL) {
                return EQUAL;
            } else if (this == GREATER_OR_EQUAL) {
                return SMALLER;
            } else if (this == SMALLER_OR_EQUAL) {
                return GREATER;
            } else if (this == GREATER) {
                return SMALLER_OR_EQUAL;
            } else if (this == SMALLER) {
                return GREATER_OR_EQUAL;
            }
            throw new RuntimeException("Opérateur non pris en charge");
        }

        public ComparisonAlgo toComparisonAlgo() {
            if (this == EQUAL) {
                return Objects::equals;
            } else if (this == NOT_EQUAL) {
                return (obj1, obj2) -> !obj1.equals(obj2);
            } else if (this == GREATER) {
                return (obj1, obj2) -> {
                    boolean result = NumberUtils.isParsable(obj1.toString()) && NumberUtils.isParsable(obj2.toString());
                    result = result && (new BigDecimal(obj1.toString()).compareTo(new BigDecimal(obj2.toString())) > 0);
                    return result;
                };
            } else if (this == GREATER_OR_EQUAL) {
                return (obj1, obj2) -> {
                    boolean result = NumberUtils.isParsable(obj1.toString()) && NumberUtils.isParsable(obj2.toString());
                    result = result && (new BigDecimal(obj1.toString()).compareTo(new BigDecimal(obj2.toString())) >= 0);
                    return result;
                };
            } else if (this == SMALLER) {
                return (obj1, obj2) -> {
                    boolean result = NumberUtils.isParsable(obj1.toString()) && NumberUtils.isParsable(obj2.toString());
                    result = result && (new BigDecimal(obj1.toString()).compareTo(new BigDecimal(obj2.toString())) < 0);
                    return result;
                };
            } else if (this == SMALLER_OR_EQUAL) {
                return (obj1, obj2) -> {
                    boolean result = NumberUtils.isParsable(obj1.toString()) && NumberUtils.isParsable(obj2.toString());
                    result = result && (new BigDecimal(obj1.toString()).compareTo(new BigDecimal(obj2.toString())) <= 0);
                    return result;
                };
            }
            throw new RuntimeException("Ne devrait pas arriver !!");
        }

    }
}
