package org.commons.dao.jdbc.query.elements;

import org.commons.dao.jdbc.query.general.ToSQL;

import java.util.Objects;

/**
 * Object JAVA représentant une table de la base de données. <br />
 *
 * @author Frédéric Moulé
 * @see ToSQL
 */
public class SQLTable extends AbstractSQLElement implements ToSQL {

    /// // Constructeur(s)

    public SQLTable(final String tableName, final String tableAlias) {
        super(null, tableName, tableAlias);
    }

    public SQLTable(final String tableName) {
        this(tableName, null);
    }

    /// // Méthode(s) de l'interface ToSQL

    public String toSQL() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append(getName());
        if (this.hasAlias()) {
            buffer.append(" AS ");
            buffer.append(this.getAlias());
        }
        return buffer.toString();
    }

    /// // Méthode(s) de la classe Object

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SQLTable sqlTable)) {
            return false;
        }
        boolean areEquals = Objects.equals(this.getPrefix(), sqlTable.getPrefix());
        areEquals = areEquals && Objects.equals(this.getName(), sqlTable.getName());
        areEquals = areEquals && Objects.equals(this.getAlias(), sqlTable.getAlias());
        return areEquals;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getPrefix(), this.getName(), this.getAlias());
    }
}
