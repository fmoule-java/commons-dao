package org.commons.dao.jdbc.query.condition;

import org.commons.dao.jdbc.query.general.SQLCondition;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

public class SQLAndCondition extends AbstractSQLCondition {
    private Set<SQLCondition> sqlConditions;

    /// // Constructeur(s)

    public SQLAndCondition(final Collection<SQLCondition> sqlConditions) {
        this.sqlConditions = new HashSet<>();
        this.sqlConditions.addAll(sqlConditions);
    }

    public SQLAndCondition(final SQLCondition... sqlConditions) {
        this(asList(sqlConditions));
    }

    /// // Méthode de l'interface SQLCondition

    @Override
    public SQLCondition negate() {
        return null;
    }

    @Override
    public String toSQL() throws SQLException {
        final StringBuilder buffer = new StringBuilder();
        if (isEmpty(sqlConditions)) {
            return buffer.toString();
        }
        int cursor = 0;
        buffer.append("(");
        for (SQLCondition sqlCondition : sqlConditions) {
            if (cursor > 0) {
                buffer.append(" AND ");
            }
            buffer.append(sqlCondition.toSQL());
            cursor++;
        }
        buffer.append(")");
        return buffer.toString();
    }
}
