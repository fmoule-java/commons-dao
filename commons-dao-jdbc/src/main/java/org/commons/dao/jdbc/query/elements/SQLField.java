package org.commons.dao.jdbc.query.elements;

import org.commons.dao.jdbc.query.general.ToSQL;

import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static org.commons.dao.jdbc.query.elements.AbstractSQLElement.SQLFieldType.STRING;

/**
 * Classe JAVA représentant les champs SQL.
 */
public class SQLField extends AbstractSQLElement implements ToSQL, Comparable<SQLField> {

    /// // Constructeur(s) :

    public SQLField(final String sqlPrefix, final String fieldName, final String sqlAlias, final SQLFieldType sqlFieldType) {
        super(sqlPrefix, fieldName, sqlAlias, sqlFieldType);
    }

    public SQLField(final String sqlPrefix, final String fieldName, final SQLFieldType sqlFieldType) {
        this(sqlPrefix, fieldName, null, sqlFieldType);
    }

    public SQLField(final String sqlPrefix, final String fieldName, final String sqlAlias) {
        super(sqlPrefix, fieldName, sqlAlias, STRING);
    }

    public SQLField(final String sqlPrefix, final String fieldName) {
        this(sqlPrefix, fieldName, STRING);
    }

    public SQLField(final String fieldName, final SQLFieldType sqlFieldType) {
        this(null, fieldName, null, sqlFieldType);
    }

    public SQLField(final String fieldName) {
        this(null, fieldName);
    }


    /**
     * Constructeur de copie
     *
     * @param sqlField : instance à copier
     */
    public SQLField(final SQLField sqlField) {
        this(requireNonNull(sqlField).getPrefix(), requireNonNull(sqlField).getName());
    }


    /// // Méthode(s) de la classe Object

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof SQLField sqlField)) {
            return false;
        }
        boolean areEquals = Objects.equals(this.getPrefix(), sqlField.getPrefix());
        areEquals &= Objects.equals(this.getName(), sqlField.getName());
        return areEquals;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getPrefix(), this.getName());
    }

    @Override
    public String toSQL() {
        final StringBuilder buffer = new StringBuilder();
        if (this.hasPrefix()) {
            buffer.append(this.getPrefix());
            buffer.append(".");
        }
        buffer.append(this.getName());
        if (this.hasAlias()) {
            buffer.append(" AS ");
            buffer.append(this.getAlias());
        }
        return buffer.toString();
    }

    /// // Méthode de l'interface Comparable

    @Override
    public int compareTo(final SQLField sqlField) {
        if (sqlField == null) {
            return 1;
        }
        if (!Objects.equals(this.getName(), sqlField.getName())) {
            return (this.getName().compareTo(sqlField.getName()));
        }
        return this.getPrefix().compareTo(sqlField.getPrefix());
    }

}
