package org.commons.dao.jdbc.query.elements;

import org.apache.commons.lang3.StringUtils;

import static org.commons.dao.jdbc.query.elements.AbstractSQLElement.SQLFieldType.STRING;

/**
 * <p>
 * Classe représentant tous les concepts SQL de base (ex : table, champs ou colonne, ..etc). <br />
 * Généralement ces éléments peuvent être écrits sous la forme suivante : <br />
 * - <i>prefix</i>.<i>nom</i> AS <i>alias</i>
 * </p>
 *
 * @author Frédéric Moulé
 */
public abstract class AbstractSQLElement {
    private static final String EMPTY_STRING = "";
    protected final SQLFieldType sqlFieldType;
    private final String sqlPrefix;
    private final String sqlName;
    private final String sqlAlias;

    /// // Constructeur(s)

    public AbstractSQLElement(final String sqlPrefix, final String sqlName, final String sqlAlias, final SQLFieldType sqlFieldType) {
        this.sqlPrefix = (sqlPrefix == null ? null : sqlPrefix.trim());
        this.sqlName = (sqlName == null ? null : sqlName.trim());
        this.sqlAlias = (sqlAlias == null ? null : sqlAlias.trim());
        this.sqlFieldType = sqlFieldType;
    }

    public AbstractSQLElement(final String sqlPrefix, final String sqlName, final String sqlAlias) {
        this(sqlPrefix, sqlName, sqlAlias, STRING);
    }

    /// // Méthode de la classe Object

    /**
     * Retourne un booléen montrant si l'instance courant possède
     * un alias SQL. <br />
     *
     * @return booléen correspondant au résultat
     */
    public boolean hasAlias() {
        return !StringUtils.isEmpty(sqlAlias);
    }

    /**
     * Permet de tester si l'instance courante possède
     * un préfix. <br />
     *
     * @return booléen montrant le résultat
     */
    public boolean hasPrefix() {
        return !StringUtils.isEmpty(sqlPrefix);
    }

    /// // Getters & Setters

    public String getName() {
        return (this.sqlName == null ? EMPTY_STRING : this.sqlName.trim());
    }

    public String getAlias() {
        return (sqlAlias == null ? EMPTY_STRING : sqlAlias.trim());
    }

    public String getPrefix() {
        return (this.sqlPrefix == null ? EMPTY_STRING : this.sqlPrefix.trim());
    }

    public SQLFieldType getType() {
        return sqlFieldType;
    }

    /**
     * <p>
     * Type des champs SQL
     * </p>
     */
    public enum SQLFieldType {
        STRING,
        INTEGER,
        LONG,
        FLOAT;
    }
}
