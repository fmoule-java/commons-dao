package org.commons.dao.jdbc.query.general;

/**
 * <p>
 * Interface représentant
 * </p>
 */
public interface SQLCondition extends ToSQL {

    SQLCondition and(final SQLCondition condition);

    SQLCondition or(final SQLCondition condition);

    SQLCondition negate();

    /// //

    static SQLCondition and(final SQLCondition... conditions) {
        SQLCondition sqlCondition = null;
        if (conditions == null) {
            return sqlCondition;
        }
        for (SQLCondition condition : conditions) {
            if (sqlCondition == null) {
                sqlCondition = condition;
                continue;
            }
            sqlCondition = sqlCondition.and(condition);
        }
        return sqlCondition;
    }

    static SQLCondition or(final SQLCondition... conditions) {
        SQLCondition sqlCondition = null;
        if (conditions == null) {
            return sqlCondition;
        }
        for (SQLCondition condition : conditions) {
            if (sqlCondition == null) {
                sqlCondition = condition;
                continue;
            }
            sqlCondition = sqlCondition.or(condition);
        }
        return sqlCondition;
    }
}
