package org.commons.dao.jdbc.query.general;

import java.sql.SQLException;

/**
 * Interface représentant toutes les instances susceptibles
 * de créer du code SQL.
 */
public interface ToSQL {

    /**
     * Retourne le code SQL. <br />
     *
     * @return code SQL
     */
    String toSQL() throws SQLException;
}
