package org.commons.dao.jdbc.query.elements;

import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.query.general.ToSQL;

import java.sql.SQLException;

import static org.commons.dao.jdbc.query.elements.SQLJoin.SQLJoinType.INNER_JOIN;

/**
 * Classe représentant les jointures SQL. <br />
 *
 * @author Frédéric Moulé
 * @see SQLCondition
 * @see SQLTable
 */
public class SQLJoin implements ToSQL {
    private SQLJoinType joinType;
    private SQLTable sqlTable;
    private SQLCondition onCondition;

    /// // Constructeur(s)

    public SQLJoin(final SQLTable sqlTable, final SQLJoinType sqlJoinType, final SQLCondition sqlCondition) {
        this.sqlTable = sqlTable;
        this.joinType = sqlJoinType;
        this.onCondition = sqlCondition;
    }

    /// // Méthode(s) statique(s)

    public static SQLJoin innerJoin(final SQLTable sqlTable, final SQLCondition onCondition) {
        return new SQLJoin(sqlTable, INNER_JOIN, onCondition);
    }

    public static SQLJoin innerJoin(final String sqlTableName, final String sqlTableAlias, final SQLCondition onCondition) {
        return new SQLJoin(new SQLTable(sqlTableName, sqlTableAlias), INNER_JOIN, onCondition);
    }

    /// // Méthode(s) de l'interface ToSQL :

    @SuppressWarnings("StringBufferReplaceableByString")
    @Override
    public String toSQL() throws SQLException {
        final StringBuilder buffer = new StringBuilder();
        buffer.append(this.joinType.toSQL());
        buffer.append(" JOIN ");
        buffer.append(this.sqlTable.toSQL());
        buffer.append(" ON (");
        buffer.append(this.onCondition.toSQL());
        buffer.append(")");
        return buffer.toString();
    }

    /// // Classe(s) interne(s)

    /**
     * <p>
     * Enumération représentant les type de jointure SQL c'est-à-dire :
     * <li>
     *     <ul>LEFT JOIN</ul>
     *     <ul>INNER JOIN</ul>
     *     <ul>RIGHT JOIN</ul>
     * </li>
     * </p>
     *
     * @author Frédéric Moulé
     */
    public enum SQLJoinType implements ToSQL {
        LEFT_JOIN("LEFT"),
        INNER_JOIN("INNER"),
        RIGHT_JOIN("RIGHT");

        private final String sqlSymbol;

        /// // Constructeur(s)

        SQLJoinType(final String sqlSymbol) {
            this.sqlSymbol = sqlSymbol;
        }

        /// // Méthode(s) de l'interface ToSQL

        @Override
        public String toSQL() {
            return this.sqlSymbol.trim().toUpperCase();
        }
    }
}
