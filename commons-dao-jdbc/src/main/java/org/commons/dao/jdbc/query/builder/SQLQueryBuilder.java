package org.commons.dao.jdbc.query.builder;

import org.apache.commons.collections4.CollectionUtils;
import org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition;
import org.commons.dao.jdbc.query.elements.QueryType;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.query.general.ToSQL;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.withFieldEqualsTo;
import static org.commons.dao.jdbc.query.elements.QueryType.*;

/**
 * <p>
 * Classe permettant la création de requête SQL
 * </p>
 *
 * @author Frédéric Moulé
 * @see ToSQL
 */
public class SQLQueryBuilder implements ToSQL {
    protected QueryType queryType;
    protected final Set<SQLField> sqlColumns;
    protected SQLTable sqlTable;
    protected final Set<SQLCondition> whereConditions;
    protected final Set<SQLFieldOperatorCondition> newFieldValues;

    /// // Constructeur(s)

    protected SQLQueryBuilder() {
        this.sqlColumns = new TreeSet<>();
        this.whereConditions = new HashSet<>();
        this.newFieldValues = new TreeSet<>();
        this.sqlTable = null;
    }

    /// // Méthode(s) de l'interface Factory

    @Override
    public String toSQL() throws SQLException {
        ToSQL sqlBuilder;
        if (queryType == SELECT) {
            sqlBuilder = new SelectQueryBuilder();
        } else if (queryType == DELETE) {
            sqlBuilder = new DeleteQueryBuilder();
        } else if (queryType == INSERT) {
            sqlBuilder = new InsertPrepapedStatementQueryBuilder();
        } else if (queryType == UPDATE) {
            sqlBuilder = new UpdateQueryBuilder();
        } else {
            throw new RuntimeException("Ne devrait pas se produire !!");
        }
        return sqlBuilder.toSQL();
    }

    @Override
    public String toString() {
        try {
            return "SQLBuilder[query = %s]".formatted(this.toSQL());
        } catch (final SQLException e) {
            return "SQLBuilder[exception = %s]".formatted(e.getMessage());
        }
    }

    /// // Méthode(s) statique(s)

    public static SQLQueryBuilder createSQLBuilder(final QueryType queryType) {
        final SQLQueryBuilder sqlQueryBuilder = new SQLQueryBuilder();
        sqlQueryBuilder.queryType = queryType;
        return sqlQueryBuilder;
    }

    public SQLQueryBuilder setTable(final SQLTable sqlTable) {
        this.sqlTable = sqlTable;
        return this;
    }

    /**
     * Ajoute une colonne dans la requête. <br />
     *
     * @param sqlColumn : colonne
     * @return instance courante du <b>builder</b>
     * @see SQLField
     * @see SQLQueryBuilder
     */
    public SQLQueryBuilder addColumn(final SQLField sqlColumn) {
        this.sqlColumns.add(sqlColumn);
        return this;
    }

    /**
     * Méthode permettant d'ajouter une clause dans la partie WHERE
     * de la requête SQL.
     *
     * @param sqlCondition : condition SQL sous forme d'une instance SQLCondition
     * @return instance du <b>builder</b>
     * @see SQLCondition
     */
    public SQLQueryBuilder addWhereCondition(final SQLCondition sqlCondition) {
        this.whereConditions.add(sqlCondition);
        return this;
    }

    public SQLQueryBuilder addNewValue(final SQLField sqlField, final Object value) {
        this.newFieldValues.add(withFieldEqualsTo(sqlField, value));
        return this;
    }

    public SQLQueryBuilder addNewValue(final String fieldName, final Object fieldValue) {
        return addNewValue(new SQLField(fieldName), fieldValue);
    }

    /// // Getters & Setters

    /**
     * Retourne la collction des colonnes
     * de l'instance courante
     *
     * @return ensemble des colonnes
     */
    public Set<SQLField> getSqlColumns() {
        return sqlColumns;
    }

    /// // Classe(s) interne(s) :

    private class SelectQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            final StringBuilder buffer = new StringBuilder();
            buffer.append("SELECT ");
            if (sqlColumns.isEmpty()) {
                buffer.append("*");
            } else {
                int cursor = 0;
                for (SQLField sqlColumn : sqlColumns) {
                    if (cursor > 0) {
                        buffer.append(", ");
                    }
                    buffer.append(sqlColumn.toSQL());
                    cursor++;
                }
            }
            buffer.append(" FROM ");
            buffer.append(sqlTable.toSQL());
            if (!whereConditions.isEmpty()) {
                buffer.append(" WHERE ");
                int cursor = 0;
                for (SQLCondition whereCondition : whereConditions) {
                    if (cursor > 0) {
                        buffer.append(" AND ");
                    }
                    buffer.append(whereCondition.toSQL());
                    cursor++;
                }
            }
            return buffer.toString();
        }
    }

    private class DeleteQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            final StringBuilder buffer = new StringBuilder();
            buffer.append("DELETE FROM ");
            buffer.append(sqlTable.toSQL());
            if (!whereConditions.isEmpty()) {
                buffer.append(" WHERE ");
                int cursor = 0;
                for (SQLCondition whereCondition : whereConditions) {
                    if (cursor > 0) {
                        buffer.append(" AND ");
                    }
                    buffer.append(whereCondition.toSQL());
                    cursor++;
                }
            }
            return buffer.toString();
        }
    }

    private class InsertPrepapedStatementQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            if (sqlTable == null) {
                throw new SQLException("La table est nulle");
            }
            if (CollectionUtils.isEmpty(newFieldValues)) {
                throw new SQLException("Les nouvelles valeurs de la requête INSERT sont vides");
            }
            final StringBuilder fieldNamesBuffer = new StringBuilder();
            final StringBuilder fieldValuesBuffer = new StringBuilder();
            int cursor = 0;
            for (SQLFieldOperatorCondition newFieldOperator : newFieldValues) {
                if (cursor > 0) {
                    fieldNamesBuffer.append(", ");
                    fieldValuesBuffer.append(", ");
                }
                fieldNamesBuffer.append(newFieldOperator.getFieldName());
                fieldValuesBuffer.append(newFieldOperator.getSQLValue());
                cursor++;
            }
            //noinspection StringBufferReplaceableByString
            final StringBuilder mainBuffer = new StringBuilder();
            mainBuffer.append("INSERT INTO ");
            mainBuffer.append(sqlTable.getName());
            mainBuffer.append(" (");
            mainBuffer.append(fieldNamesBuffer);
            mainBuffer.append(") VALUES (");
            mainBuffer.append(fieldValuesBuffer);
            mainBuffer.append(")");
            return mainBuffer.toString();
        }
    }

    private class UpdateQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            final StringBuilder mainBuffer = new StringBuilder();
            mainBuffer.append("UPDATE ");
            mainBuffer.append(sqlTable.getName());
            mainBuffer.append(" SET ");
            int cursor = 0;
            for (SQLFieldOperatorCondition newFieldValueCondition : newFieldValues) {
                if (cursor > 0) {
                    mainBuffer.append(", ");
                }
                mainBuffer.append(newFieldValueCondition.getFieldName());
                mainBuffer.append(" = ");
                mainBuffer.append(newFieldValueCondition.getSQLValue());
                cursor++;
            }
            mainBuffer.append(" WHERE ");
            cursor = 0;
            for (SQLCondition whereCondition : whereConditions) {
                if (cursor > 0) {
                    mainBuffer.append(" AND ");
                }
                mainBuffer.append(whereCondition.toSQL());
                cursor++;
            }
            return mainBuffer.toString();
        }
    }
}
