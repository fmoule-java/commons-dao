package org.commons.dao.jdbc.query.condition;

import org.commons.dao.jdbc.query.general.SQLCondition;

import java.sql.SQLException;

public class DirectSQLCondition extends AbstractSQLCondition {
    private String sqlCode;

    /// // Constructeur(s)

    private DirectSQLCondition(String sqlCode) {
        this.sqlCode = sqlCode;
    }

    /// // Méthode(s) de la classe AbstractSQLCondition

    @Override
    public String toSQL() throws SQLException {
        return this.sqlCode;
    }

    @Override
    public SQLCondition negate() {
        throw new RuntimeException("Pas encore implémenté");
    }

    /// // Méthodez(s) statique(s)

    public static DirectSQLCondition withDirectSQLCondition(final String sqlCode) {
        return new DirectSQLCondition(sqlCode);
    }
}
