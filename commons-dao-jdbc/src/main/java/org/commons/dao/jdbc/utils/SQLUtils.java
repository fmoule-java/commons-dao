package org.commons.dao.jdbc.utils;

import org.commons.dao.transcoder.Transcoder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;

public class SQLUtils {

    /// // Méthode(s) statique(s)

    public static int executeUpdateSQLQuery(final Connection connection, final String sql) throws SQLException {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            return statement.executeUpdate(sql);
        } finally {
            closeQuietly(statement);
        }
    }

    public static int executeUpdateSQLQuery(final DataSource dataSource, final String sql) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            return statement.executeUpdate(sql);
        } finally {
            closeQuietly(statement);
            closeQuietly(connection);
        }
    }

    public static <T> List<T> executeSelectQuery(final DataSource dataSource,
                                                 final String sql,
                                                 final Transcoder<ResultSet, T> resultSetTranscoder) throws Exception {
        if (dataSource == null) {
            throw new SQLException("l'instance dataSource est nulle");
        }
        if (isEmpty(sql)) {
            throw new SQLException("La requête SQL est nulle ou vide");
        }
        if (resultSetTranscoder == null) {
            throw new SQLException("Le transcoder est nul");
        }
        final List<T> beans = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                beans.add(resultSetTranscoder.transcode(resultSet));
            }
            return beans;
        } finally {
            closeQuietly(statement);
            closeQuietly(resultSet);
            closeQuietly(connection);
        }
    }

    /// // Méthode(s) statique(s) :

    public static String escapeSpecialCharacters(final String fieldValue) {
        if (isEmpty(fieldValue)) {
            return fieldValue;
        }
        return fieldValue.replaceAll("\'", "\'\'").trim();
    }
}
