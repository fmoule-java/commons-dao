package org.commons.dao.jdbc.query.condition;

import org.commons.dao.jdbc.query.general.SQLCondition;

/**
 * Classe abstraite de base utilisée par toutes les implémentations
 * de l'interface SQLCondition.
 *
 * @author Frédéric Moulé
 * @see SQLCondition
 */
public abstract class AbstractSQLCondition implements SQLCondition {

    /// // Méthode(s) de l'interface SQLCondition

    @Override
    public SQLCondition and(final SQLCondition condition) {
        if (condition == null) {
            return this;
        }
        return new SQLAndCondition(this, condition);
    }

    @Override
    public SQLCondition or(final SQLCondition condition) {
        return null;
    }
}
