package org.commons.dao.jdbc;

import org.apache.commons.collections4.CollectionUtils;
import org.commons.dao.base.AbstractDAO;
import org.commons.dao.bean.AbstractDAOBean.IsNewPredicate;
import org.commons.dao.jdbc.beans.AbstractJDBCDAOBean;
import org.commons.dao.jdbc.predicate.JDBCPredicate;
import org.commons.dao.jdbc.query.builder.SQLQueryBuilder;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;

import javax.sql.DataSource;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static java.sql.Statement.RETURN_GENERATED_KEYS;
import static java.util.Objects.requireNonNull;
import static org.commons.dao.jdbc.utils.SQLUtils.executeSelectQuery;
import static org.commons.dao.utils.CollectionUtils.filterCollection;
import static org.commons.dao.utils.ObjectUtils.cast;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;
import static org.commons.dao.utils.reflection.ReflectionUtils.getGetter;

/**
 * DAO basé sur les connexions JDBC et l'API
 *
 * @param <T> : Type des objets manipulés par le DAO
 */
public abstract class AbstractJDBCDAO<T extends AbstractJDBCDAOBean<?>> extends AbstractDAO<T> {
    private final DataSource dataSource;
    private final SQLTable mainSqlTable;
    private final Class<T> beanClass;

    /// // Constructeur(s)

    public AbstractJDBCDAO(final DataSource dataSource, final SQLTable mainSqlTable, final Class<T> beanClass) {
        this.dataSource = requireNonNull(dataSource, "La source de données ne doit pas être nulle");
        this.mainSqlTable = requireNonNull(mainSqlTable, "La table du DAO n'est pas spécifiée");
        this.beanClass = requireNonNull(beanClass, "La classe des objets manipulés ne doit pas être nulle");
    }

    /// // Méthode(s) générale(s)

    /**
     * Méthode permettant de construire ou récupérer une connexion JDBC
     *
     * @return connexion JDBC
     */
    public Connection getConnection() throws SQLException {
        final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    /// // Méthode(s) de la classe AbstractDAO :

    public abstract SQLQueryBuilder buildSelectQuery(final JDBCPredicate<T> predicate) throws SQLException;

    public abstract SQLQueryBuilder buildInsertBeanQuery() throws SQLException;

    public abstract SQLQueryBuilder buildUpdateBeanQuery() throws SQLException;

    public abstract T transcodeResultSet(final ResultSet resultSet) throws SQLException;

    @SuppressWarnings("SqlSourceToSinkFlow")
    @Override
    public List<T> select(final Predicate<T> predicate) throws Exception {
        final List<T> resultBeans = new ArrayList<>();
        final Connection connection = this.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            if (predicate instanceof JDBCPredicate<T> jdbcPredicate) {
                statement.execute(buildSelectQuery(jdbcPredicate).toSQL());
                resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    resultBeans.add(transcodeResultSet(resultSet));
                }
                return resultBeans;
            } else {
                statement.execute(buildSelectQuery(null).toSQL());
                resultSet = statement.getResultSet();
                T foundBean;
                while (resultSet.next()) {
                    foundBean = transcodeResultSet(resultSet);
                    if (!predicate.test(foundBean)) {
                        continue;
                    }
                    resultBeans.add(foundBean);
                }
                return resultBeans;
            }

        } catch (Exception exception) {
            this.getLogger().error(exception.getMessage(), exception);
            throw exception;
        } finally {
            closeQuietly(resultSet);
            closeQuietly(statement);
            closeQuietly(connection);
        }
    }

    @Override
    public long selectCount() throws Exception {
        final List<Long> results = executeSelectQuery(this.dataSource,
                "SELECT COUNT(*) FROM " + this.mainSqlTable.getName(),
                resultSet -> resultSet.getLong(1));
        if (results == null || results.isEmpty()) {
            return 0;
        }
        return results.getFirst();
    }

    @Override
    public T save(final T initBean) throws Exception {
        final Collection<T> savedBeans = this.save(List.of(initBean));
        if (savedBeans == null || savedBeans.isEmpty()) {
            return null;
        }
        return savedBeans.iterator().next();
    }

    @Override
    public Collection<T> save(final Collection<? extends T> beans) throws Exception {
        final List<T> savedBeans = new ArrayList<>();
        if (CollectionUtils.isEmpty(beans)) {
            return savedBeans;
        }
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeysResultSet = null;
        try {
            connection = this.getConnection();
            SQLQueryBuilder sqlBuilder;
            T savedBean;
            Method getter;

            // Insertion
            SQLQueryBuilder sqlQueryBuilder = buildInsertBeanQuery();
            //noinspection SqlSourceToSinkFlow
            statement = connection.prepareStatement(sqlQueryBuilder.toSQL(), RETURN_GENERATED_KEYS);
            final List<T> initBeans = filterCollection(beans, new IsNewPredicate<>());
            int index;
            for (T initBean : initBeans) {
                index = 1;
                for (SQLField sqlColumn : sqlQueryBuilder.getSqlColumns()) {
                    if (Objects.equals(initBean.getIdField(), sqlColumn)) {
                        continue;
                    }
                    getter = getGetter(initBean.getClass(), sqlColumn.getName());
                    if (getter == null) {
                        throw new SQLException("Il manque le getter pour le champs '%s'".formatted(sqlColumn.getName()));
                    }
                    statement.setObject(index, getter.invoke(initBean));
                    index++;
                }
                statement.addBatch();
            }
            statement.executeBatch();
            generatedKeysResultSet = statement.getGeneratedKeys();
            index = 0;
            while (generatedKeysResultSet.next()) {
                if (generatedKeysResultSet.wasNull()) {
                    throw new SQLException("Aucun identifiant n'a été généré");
                }
                savedBean = cast(initBeans.get(index).clone(), new SQLException("La fonction clone() renvoie une mauvaise instance"));
                savedBean.setId(cast(generatedKeysResultSet.getObject(1), new SQLException("L'id généré n'a pas le bon type")));
                savedBeans.add(savedBean);
                index++;
            }

            // Update :
            sqlBuilder = buildUpdateBeanQuery();
            //noinspection SqlSourceToSinkFlow
            statement = connection.prepareStatement(sqlBuilder.toSQL());
            for (T initBeanToUpdate : filterCollection(beans, new IsNewPredicate<>().negate())) {
                index = 1;
                for (SQLField sqlColumn : sqlQueryBuilder.getSqlColumns()) {
                    getter = getGetter(initBeanToUpdate.getClass(), sqlColumn.getName());
                    if (getter == null) {
                        throw new SQLException("Il manque le getter pour le champs '%s'".formatted(sqlColumn.getName()));
                    }
                    statement.setObject(index, getter.invoke(initBeanToUpdate));
                    savedBeans.add(cast(initBeanToUpdate.clone(), new SQLException("La fonction clone() renvoie une mauvaise instance")));
                    index++;
                }
                getter = getGetter(initBeanToUpdate.getClass(), initBeanToUpdate.getIdField().getName());
                statement.setObject(index, getter.invoke(initBeanToUpdate));
                statement.addBatch();
            }
            statement.executeBatch();

            connection.commit();
            return savedBeans;
        } catch (Exception exception) {
            this.getLogger().error(exception.getMessage(), exception);
            if (connection != null) {
                connection.rollback();
            }
            throw exception;
        } finally {
            closeQuietly(generatedKeysResultSet);
            closeQuietly(statement);
            closeQuietly(connection);
        }
    }

    @Override
    public void delete(final Predicate<T> predicate) throws Exception {

    }

    @Override
    public boolean isClosed() {
        return false;
    }


}
