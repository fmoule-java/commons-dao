package org.commons.dao.jdbc.query.elements;

/**
 * Enumératioh listant les types de requête SQL.
 *
 * @author Frédéric Moulé
 */
public enum QueryType {
    SELECT,
    INSERT,
    UPDATE,
    DELETE;

    /// // Getters & Setters :

    public String getSQLName() {
        return this.name();
    }
}
