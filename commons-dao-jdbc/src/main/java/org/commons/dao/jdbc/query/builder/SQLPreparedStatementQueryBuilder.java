package org.commons.dao.jdbc.query.builder;

import org.apache.commons.collections4.CollectionUtils;
import org.commons.dao.jdbc.query.elements.QueryType;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.query.general.ToSQL;

import java.sql.SQLException;

import static org.commons.dao.jdbc.query.elements.QueryType.INSERT;
import static org.commons.dao.jdbc.query.elements.QueryType.UPDATE;

public class SQLPreparedStatementQueryBuilder extends SQLQueryBuilder {

    @Override
    public SQLQueryBuilder addNewValue(final SQLField sqlField, Object value) {
        throw new RuntimeException("Non utilisable car on utilise dans les requête SQL le mot clé ?");
    }

    @Override
    public SQLQueryBuilder addNewValue(final String fieldName, Object fieldValue) {
        throw new RuntimeException("Non utilisable car on utilise dans les requête SQL le mot clé ?");
    }

    @Override
    public String toSQL() throws SQLException {
        ToSQL sqlBuilder;
        if (queryType == INSERT) {
            sqlBuilder = new InsertPreparedStatamentQueryBuilder();
            return sqlBuilder.toSQL();
        } else if (queryType == UPDATE) {
            sqlBuilder = new UpdatePreparedStatementQueryBuilder();
            return sqlBuilder.toSQL();
        } else {
            return super.toSQL();
        }
    }

    /// // Méthode(s) statique(s)

    public static SQLPreparedStatementQueryBuilder createSQLreparedStatementBuilder(final QueryType queryType) {
        final SQLPreparedStatementQueryBuilder sqlQueryBuilder = new SQLPreparedStatementQueryBuilder();
        sqlQueryBuilder.queryType = queryType;
        return sqlQueryBuilder;
    }

    /// // Classe(s) interne(s)

    private class InsertPreparedStatamentQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            final StringBuilder buffer = new StringBuilder();
            buffer.append("INSERT INTO ");
            buffer.append(sqlTable.getName());
            if (CollectionUtils.isEmpty(sqlColumns)) {
                throw new SQLException("Pas de colonnes spécifiées");
            }
            buffer.append(" (");
            int cursor = 0;
            for (SQLField sqlField : sqlColumns) {
                if (cursor > 0) {
                    buffer.append(", ");
                }
                buffer.append(sqlField.getName());
                cursor++;
            }
            buffer.append(") VALUES (");
            cursor = 0;
            for (int i = 0; i < sqlColumns.size(); i++) {
                if (cursor > 0) {
                    buffer.append(", ");
                }
                buffer.append("?");
                cursor++;
            }
            buffer.append(")");
            return buffer.toString();
        }
    }

    private class UpdatePreparedStatementQueryBuilder implements ToSQL {

        @Override
        public String toSQL() throws SQLException {
            if (CollectionUtils.isEmpty(sqlColumns)) {
                throw new SQLException("Pas de colonnes spécifiées");
            }
            final StringBuilder buffer = new StringBuilder();
            buffer.append("UPDATE ");
            buffer.append(sqlTable.getName());
            buffer.append(" SET ");
            int cursor = 0;
            for (SQLField sqlField : sqlColumns) {
                if (cursor > 0) {
                    buffer.append(", ");
                }
                buffer.append(sqlField.getName());
                buffer.append(" = ?");
                cursor++;
            }
            if (CollectionUtils.isEmpty(sqlColumns)) {
                throw new SQLException("Il manque une clause WHERE");
            }
            buffer.append(" WHERE ");
            cursor = 0;
            for (SQLCondition whereCondition : whereConditions) {
                if (cursor > 0) {
                    buffer.append(" AND ");
                }
                buffer.append(whereCondition.toSQL());
                cursor++;
            }
            return buffer.toString();
        }
    }
}
