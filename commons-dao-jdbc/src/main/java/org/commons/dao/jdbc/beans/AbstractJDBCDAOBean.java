package org.commons.dao.jdbc.beans;

import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jdbc.query.elements.SQLField;

public abstract class AbstractJDBCDAOBean<K> extends AbstractDAOBean<K> {
    private final IdSQLField<K> idField;

    /// // Constructeur(s)

    protected AbstractJDBCDAOBean(final String idFieldName) {
        this.idField = new IdSQLField<>(idFieldName, null);
    }

    protected AbstractJDBCDAOBean() {
        this("id");
    }

    /// // Getters & Setters

    @Override
    public void setId(final K idValue) {
        this.idField.setIdValue(idValue);
    }

    @Override
    public K getId() {
        return this.idField.getIdValue();
    }

    public SQLField getIdField() {
        return idField;
    }

    /// // Classe(s) interne(s)

    public static class IdSQLField<K> extends SQLField {
        private K idValue;

        /// // Constructeur(s)

        public IdSQLField(final String fieldName, final K idValue) {
            super(fieldName);
            this.idValue = idValue;
        }

        /// // Getters & Setters

        public K getIdValue() {
            return idValue;
        }

        public void setIdValue(final K idValue) {
            this.idValue = idValue;
        }
    }
}
