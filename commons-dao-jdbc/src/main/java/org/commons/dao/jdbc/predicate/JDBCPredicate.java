package org.commons.dao.jdbc.predicate;

import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.query.general.ToSQL;

import java.sql.SQLException;
import java.util.function.Predicate;

/**
 * <p>
 * Cette interface représente tous les prédicats utilisés par les DAO JDBC.
 * </p>
 *
 * @param <T> : type des objets manipulés
 * @author Frédéric Moulé
 * @see Predicate
 * @see ToSQL
 */
public interface JDBCPredicate<T> extends Predicate<T>, ToSQL {

    /**
     * Retourne la condition SQL correspondant au prédicat. <br />
     *
     * @return condition SQL
     * @see SQLCondition
     */
    SQLCondition where();

    /// // Méthode par défaut :

    @Override
    default String toSQL() throws SQLException {
        return this.where().toSQL();
    }
}
