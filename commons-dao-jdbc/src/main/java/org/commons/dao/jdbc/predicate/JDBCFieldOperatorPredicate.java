package org.commons.dao.jdbc.predicate;

import org.commons.dao.jdbc.beans.AbstractJDBCDAOBean;
import org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.general.SQLCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.EQUAL;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.withFieldOperator;
import static org.commons.dao.utils.reflection.ReflectionUtils.getGetter;

/**
 * Prédicat JDBC
 *
 * @param <T> : type des objets manipulés par le prédicat
 * @see JDBCPredicate
 */
public class JDBCFieldOperatorPredicate<T extends AbstractJDBCDAOBean<?>> implements JDBCPredicate<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCFieldOperatorPredicate.class);
    private final SQLField sqlField;
    private final SQLOperator sqlOperateur;
    private final Object value;

    /// // Constructeur(s)

    /**
     * Constructeur.
     *
     * @param sqlField     : champs SQL
     * @param sqlOperateur :  opérateur
     * @param value        valeur initiale
     */
    JDBCFieldOperatorPredicate(final SQLField sqlField,
                               final SQLOperator sqlOperateur,
                               final Object value) {
        this.sqlField = sqlField;
        this.sqlOperateur = sqlOperateur;
        this.value = value;
    }

    /// // Méthode(s) de l'interface JDBCPredicate

    @Override
    public boolean test(final T bean) {
        if (bean == null) {
            return false;
        }
        final Method getter = getGetter(bean.getClass(), sqlField.getName());
        if (getter == null && value == null) {
            return true;
        } else if (getter == null) {
            return false;
        }
        try {
            final Object beanValue = getter.invoke(bean);
            if (beanValue == null) {
                return value == null;
            }
            return sqlOperateur.toComparisonAlgo().compare(beanValue, value);
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            return false;
        }
    }

    @Override
    public SQLCondition where() {
        return withFieldOperator(this.sqlField, this.sqlOperateur, value);
    }

    /// // Méthode(s) statique(s)

    public static <B extends AbstractJDBCDAOBean<?>> JDBCFieldOperatorPredicate<B> withJDBCFieldEqualTo(final String fieldName, final Object sqlFieldValue) {
        return new JDBCFieldOperatorPredicate<>(new SQLField(fieldName), EQUAL, sqlFieldValue);
    }
}
