package org.commons.dao.jdbc.utils;

import org.commons.dao.jdbc.AbstractJDBCDAOTestIT;
import org.commons.dao.jdbc.query.builder.SQLQueryBuilder;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.builder.SQLQueryBuilder.createSQLBuilder;
import static org.commons.dao.jdbc.query.elements.QueryType.INSERT;
import static org.commons.dao.jdbc.utils.SQLUtils.executeUpdateSQLQuery;

public class SQLUtilsTestIT extends AbstractJDBCDAOTestIT {

    /// // Tests fonctionnels

    @Test
    public void shouldInsert() throws Exception {

        // Récupération de l'instance DataSource
        final DataSource dataSource = this.loadDataSource();
        assertThat(dataSource).isNotNull();

        // Création de la requête SQL
        final SQLQueryBuilder queryBuilder = createSQLBuilder(INSERT);
        queryBuilder.setTable(new SQLTable("country"));
        queryBuilder.addNewValue("name", "allemagne");
        assertThat(executeUpdateSQLQuery(dataSource, queryBuilder.toSQL())).isEqualTo(1);
    }


}