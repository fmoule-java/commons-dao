package org.commons.dao.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.BeforeEach;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static org.commons.dao.jdbc.utils.SQLUtils.executeUpdateSQLQuery;

public class AbstractJDBCDAOTestIT {
    private static final String DEFAULT_PROPERTIES_FILE_PATH = AbstractJDBCDAOTestIT.class.getResource("/application.properties").getPath();
    protected Properties testProperties;

    /// // Constructeur(s)

    public AbstractJDBCDAOTestIT() {
        testProperties = new Properties();
    }

    protected DataSource loadDataSource() throws IOException {

        // Chargement du fichier des propriétés
        this.testProperties.load(new FileReader(DEFAULT_PROPERTIES_FILE_PATH));

        // Création de la source de données :
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.testProperties.getProperty("db.driver"));
        hikariConfig.setJdbcUrl(this.testProperties.getProperty("db.url"));
        hikariConfig.setUsername(this.testProperties.getProperty("db.username"));
        hikariConfig.setPassword(this.testProperties.getProperty("db.password"));
        hikariConfig.setMaximumPoolSize(5);
        return new HikariDataSource(hikariConfig);
    }

    /// // Initialisation

    @BeforeEach
    void setUp() throws Exception {
        final DataSource dataSource = this.loadDataSource();
        executeUpdateSQLQuery(dataSource, "DELETE FROM country");
        executeUpdateSQLQuery(dataSource, "DELETE FROM company");
        executeUpdateSQLQuery(dataSource, "DELETE FROM person");
    }
}