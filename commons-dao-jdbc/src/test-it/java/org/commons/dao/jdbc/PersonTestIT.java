package org.commons.dao.jdbc;

import org.commons.dao.jdbc.predicate.JDBCPredicate;
import org.commons.dao.jdbc.query.beans.PersonBean;
import org.commons.dao.jdbc.query.builder.SQLQueryBuilder;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.predicate.JDBCFieldOperatorPredicate.withJDBCFieldEqualTo;
import static org.commons.dao.jdbc.query.builder.SQLPreparedStatementQueryBuilder.createSQLreparedStatementBuilder;
import static org.commons.dao.jdbc.query.builder.SQLQueryBuilder.createSQLBuilder;
import static org.commons.dao.jdbc.query.condition.DirectSQLCondition.withDirectSQLCondition;
import static org.commons.dao.jdbc.query.elements.AbstractSQLElement.SQLFieldType.LONG;
import static org.commons.dao.jdbc.query.elements.AbstractSQLElement.SQLFieldType.STRING;
import static org.commons.dao.jdbc.query.elements.QueryType.*;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;

public class PersonTestIT extends AbstractJDBCDAOTestIT {
    private AbstractJDBCDAO<PersonBean> jdbcPersonDAO;

    @BeforeEach
    void setUp() throws Exception {

        // Appel à la classe mère
        super.setUp();

        // Construction du DAO :
        jdbcPersonDAO = new JDBCPersonDAO(this.loadDataSource());
    }

    @AfterEach
    void tearDown() {
        closeQuietly(this.jdbcPersonDAO);
    }

    /// // Tests unitaires

    @Test
    public void shouldInsertAndSelect() throws Exception {
        final PersonBean initPersonBean = new PersonBean("frederic", "moule");
        assertThat(initPersonBean.getId()).isNull();
        final PersonBean savedPersonBean = jdbcPersonDAO.save(initPersonBean);
        assertThat(savedPersonBean).isNotNull();
        assertThat(savedPersonBean.getId()).isNotNull();
        assertThat(initPersonBean.getId()).isNull();

        // On compte
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(1);
    }


    @Test
    public void shouldSaveACollection() throws Exception {
        final List<PersonBean> personBeans = new ArrayList<>();
        personBeans.add(new PersonBean("frederic", "moule"));
        personBeans.add(new PersonBean("gael", "lebreton"));
        personBeans.add(new PersonBean("laurent", "pechereau"));
        this.jdbcPersonDAO.save(personBeans);

        // Vérification
        assertThat(this.jdbcPersonDAO.selectCount()).isEqualTo(3);
    }

    @Test
    public void shouldGetUpdateQueryBuilder() throws Exception {
        final SQLQueryBuilder queryBuilder = this.jdbcPersonDAO.buildUpdateBeanQuery();
        assertThat(queryBuilder).isNotNull();
        assertThat(queryBuilder.toSQL()).isEqualTo("UPDATE person SET company_id = ?, first_name = ?, last_name = ? WHERE id = ?");
    }


    @Test
    public void shouldUpdate() throws Exception {

        // Sauvegarde
        PersonBean savedPersonBean = jdbcPersonDAO.save(new PersonBean("gael", "lebreton"));
        assertThat(savedPersonBean).isNotNull();
        final Long initId = savedPersonBean.getId();
        assertThat(initId).isNotNull();
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(1);

        // Mis à jour
        savedPersonBean.setLastName("lebreton-dubuc");
        savedPersonBean = jdbcPersonDAO.save(savedPersonBean);
        assertThat(savedPersonBean).isNotNull();
        assertThat(savedPersonBean.getId()).isEqualTo(initId);
        assertThat(savedPersonBean.getLastName()).isEqualTo("lebreton-dubuc");
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(1);
    }


    @Test
    public void shouldInsertUpdateAndSelect() throws Exception {

        // Sauvegarde
        PersonBean savedGael = jdbcPersonDAO.save(new PersonBean("gael", "lebreton"));
        assertThat(savedGael).isNotNull();
        final Long initId = savedGael.getId();
        assertThat(initId).isNotNull();
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(1);

        // Update ° Insert
        final List<PersonBean> personBeans = new ArrayList<>();
        savedGael.setLastName("lebreton-dubuc");
        personBeans.add(savedGael);
        personBeans.add(new PersonBean("frederic", "moule"));
        this.jdbcPersonDAO.save(personBeans);

        // On vérifie la base de données
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(2);
        List<PersonBean> foundPersonBeans = jdbcPersonDAO.select(withJDBCFieldEqualTo("id", savedGael.getId()));
        assertThat(foundPersonBeans).isNotNull();
        assertThat(foundPersonBeans.size()).isEqualTo(1);
        PersonBean foundPerson = foundPersonBeans.getFirst();
        assertThat(foundPerson).isNotNull();
        assertThat(foundPerson.getFirstName()).isEqualTo("gael");
        assertThat(foundPerson.getLastName()).isEqualTo("lebreton-dubuc");
        foundPersonBeans = jdbcPersonDAO.select(withJDBCFieldEqualTo("first_name", "frederic"));
        assertThat(foundPersonBeans).isNotNull();
        assertThat(foundPersonBeans.size()).isEqualTo(1);
        foundPerson = foundPersonBeans.getFirst();
        assertThat(foundPerson.getId()).isNotNull();
        assertThat(foundPerson.getFirstName()).isEqualTo("frederic");
        assertThat(foundPerson.getLastName()).isEqualTo("moule");
    }

    @Test
    public void shouldSelectWithNoJDBCPredicate() throws Exception {

        // Sauvegarde
        List<PersonBean> personBeans = new ArrayList<>();
        personBeans.add(new PersonBean("frederic", "moule"));
        personBeans.add(new PersonBean("laurent", "pechereau"));
        personBeans.add(new PersonBean("david", "pechereau"));
        personBeans.add(new PersonBean("gael", "lebreton"));
        jdbcPersonDAO.save(personBeans);
        assertThat(jdbcPersonDAO.selectCount()).isEqualTo(4);

        final List<PersonBean> foundPersonBeans = this.jdbcPersonDAO.select(pers -> "pechereau".equals(pers.getLastName()));
        assertThat(foundPersonBeans).isNotNull();
        assertThat(foundPersonBeans.size()).isEqualTo(2);
        for (PersonBean foundPersonBean : foundPersonBeans) {
            assertThat("david".equals(foundPersonBean.getFirstName()) || "laurent".equals(foundPersonBean.getFirstName())).isTrue();
            assertThat(foundPersonBean.getLastName()).isEqualTo("pechereau");
        }
    }

    @Test
    public void shouldSelectAll() throws Exception {

        // Sauvegarde
        List<PersonBean> personBeans = new ArrayList<>();
        personBeans.add(new PersonBean("frederic", "moule"));
        personBeans.add(new PersonBean("laurent", "pechereau"));
        personBeans.add(new PersonBean("david", "pechereau"));
        personBeans.add(new PersonBean("gael", "lebreton"));
        jdbcPersonDAO.save(personBeans);

        final TreeSet<PersonBean> foundPersonBeans = new TreeSet<>((person1, person2) -> {
            if (person1 == null && person2 == null) {
                return 0;
            } else if (person1 == null) {
                return -1;
            } else if (person2 == null) {
                return 1;
            }
            int comp = person1.getLastName().compareTo(person2.getLastName());
            if (comp != 0) {
                return comp;
            }
            return person1.getFirstName().compareTo(person2.getFirstName());
        });
        foundPersonBeans.addAll(jdbcPersonDAO.selectAll());
        assertThat(foundPersonBeans).isNotNull();
        assertThat(foundPersonBeans.size()).isEqualTo(4);
        assertThat(foundPersonBeans.removeFirst().isFunctionnallyEquals(new PersonBean("gael", "lebreton"))).isTrue();
        assertThat(foundPersonBeans.removeFirst().isFunctionnallyEquals(new PersonBean("frederic", "moule"))).isTrue();
        assertThat(foundPersonBeans.removeFirst().isFunctionnallyEquals(new PersonBean("david", "pechereau"))).isTrue();
        assertThat(foundPersonBeans.removeFirst().isFunctionnallyEquals(new PersonBean("laurent", "pechereau"))).isTrue();
    }

    /// // Classe(s) interne(s)

    public static class JDBCPersonDAO extends AbstractJDBCDAO<PersonBean> {
        private static final SQLTable PERSON_TABLE = new SQLTable("person");

        public JDBCPersonDAO(final DataSource dataSource) {
            super(dataSource, PERSON_TABLE, PersonBean.class);
        }

        @Override
        public PersonBean transcodeResultSet(final ResultSet resultSet) throws SQLException {
            final PersonBean personBean = new PersonBean();
            personBean.setId(resultSet.getLong("id"));
            personBean.setFirstName(resultSet.getString("first_name"));
            personBean.setLastName(resultSet.getString("last_name"));
            return personBean;
        }

        @Override
        public SQLQueryBuilder buildSelectQuery(final JDBCPredicate<PersonBean> predicate) throws SQLException {
            final SQLQueryBuilder sqlBuilder = createSQLBuilder(SELECT);
            sqlBuilder.setTable(PERSON_TABLE);
            sqlBuilder.addColumn(new SQLField("id"));
            sqlBuilder.addColumn(new SQLField("first_name"));
            sqlBuilder.addColumn(new SQLField("last_name"));
            sqlBuilder.addColumn(new SQLField("company_id"));
            if (predicate != null) {
                sqlBuilder.addWhereCondition(predicate.where());
            }
            return sqlBuilder;
        }

        @Override
        public SQLQueryBuilder buildInsertBeanQuery() throws SQLException {
            Set<SQLField> fields = new HashSet<>();
            fields.add(new SQLField("first_name", STRING));
            fields.add(new SQLField("last_name", STRING));
            fields.add(new SQLField("company_id", LONG));
            final SQLQueryBuilder sqlBuilder = createSQLreparedStatementBuilder(INSERT);
            sqlBuilder.setTable(PERSON_TABLE);
            for (SQLField field : fields) {
                sqlBuilder.addColumn(field);
            }
            return sqlBuilder;
        }

        @Override
        public SQLQueryBuilder buildUpdateBeanQuery() {
            final SQLQueryBuilder sqlBuilder = createSQLreparedStatementBuilder(UPDATE);
            sqlBuilder.setTable(PERSON_TABLE);
            sqlBuilder.addColumn(new SQLField("first_name", STRING));
            sqlBuilder.addColumn(new SQLField("last_name", STRING));
            sqlBuilder.addColumn(new SQLField("company_id", LONG));
            sqlBuilder.addWhereCondition(withDirectSQLCondition("id = ?"));
            return sqlBuilder;
        }
    }
}
