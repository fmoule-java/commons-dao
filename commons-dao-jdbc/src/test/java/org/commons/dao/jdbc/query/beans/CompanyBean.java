package org.commons.dao.jdbc.query.beans;

import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Objects;

public class CompanyBean extends AbstractDAOBean<Long> {
    private Long id;
    private String name;

    /// // Constructeur(s)

    public CompanyBean(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public CompanyBean(final String name) {
        this(null, name);
    }

    /// // Méthode(s) de la classe AbstractDAOBean :

    @Override
    public int functionnalHashCode() {
        return Objects.hashCode(this.getName());
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof CompanyBean companyBean)) {
            return false;
        }
        return Objects.equals(this.getName(), companyBean.getName());
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonCompany = new JSONObject();
        jsonCompany.put("id", getId());
        jsonCompany.put("id", getName());
        return jsonCompany;
    }

    /// // Getters & Setters

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return (name == null ? "" : name.trim());
    }
}
