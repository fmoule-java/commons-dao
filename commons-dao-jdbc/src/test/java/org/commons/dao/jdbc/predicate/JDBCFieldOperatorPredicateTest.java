package org.commons.dao.jdbc.predicate;

import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jdbc.beans.AbstractJDBCDAOBean;
import org.commons.dao.jdbc.query.elements.SQLField;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.*;

class JDBCFieldOperatorPredicateTest {

    /// // Tests unitaires

    @Test
    public void shouldTestIfGreater() {
        final Predicate<NumericTestBean> operatorPredicate = new JDBCFieldOperatorPredicate<>(new SQLField("number"), GREATER, 2);
        assertThat(operatorPredicate.test(new NumericTestBean(3))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(0.6))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(2))).isFalse();
        assertThat(operatorPredicate.test(null)).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("-7.8")))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("14.89")))).isTrue();
    }

    @Test
    public void shouldTestIfGreaterOrEqual() {
        final Predicate<NumericTestBean> operatorPredicate = new JDBCFieldOperatorPredicate<>(new SQLField("number"), GREATER_OR_EQUAL, 2);
        assertThat(operatorPredicate.test(new NumericTestBean(3))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(0.6))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(2))).isTrue();
        assertThat(operatorPredicate.test(null)).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("-7.8")))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("14.89")))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("2")))).isTrue();
    }

    @Test
    public void shouldTestIfSmaller() {
        final Predicate<NumericTestBean> operatorPredicate = new JDBCFieldOperatorPredicate<>(new SQLField("number"), SMALLER, 2);
        assertThat(operatorPredicate.test(new NumericTestBean(3))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(1.87))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(2))).isFalse();
        assertThat(operatorPredicate.test(null)).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("-7.8")))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("14.89")))).isFalse();
    }

    @Test
    public void shouldTestIfSmallerOrEqual() {
        final Predicate<NumericTestBean> operatorPredicate = new JDBCFieldOperatorPredicate<>(new SQLField("number"), SMALLER_OR_EQUAL, 2);
        assertThat(operatorPredicate.test(new NumericTestBean(3))).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(1.6))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(2))).isTrue();
        assertThat(operatorPredicate.test(null)).isFalse();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("-5.6")))).isTrue();
        assertThat(operatorPredicate.test(new NumericTestBean(new BigDecimal("14.89")))).isFalse();
    }

    @Test
    public void shouldWhereCondition() throws Exception {
        JDBCPredicate<AbstractJDBCDAOBean<?>> predicate = new JDBCFieldOperatorPredicate<>(new SQLField("number"), EQUAL, 2);
        assertThat(predicate.where()).isNotNull();
        assertThat(predicate.where().toSQL()).isEqualTo("number = 2");
        predicate = new JDBCFieldOperatorPredicate<>(new SQLField("a", "number"), GREATER, 4.5);
        assertThat(predicate.where()).isNotNull();
        assertThat(predicate.where().toSQL()).isEqualTo("a.number > 4.5");
    }

    /// // Classe(s) interne(s)

    @SuppressWarnings("unused")
    private static class NumericTestBean extends AbstractJDBCDAOBean<Long> {
        private Number number;

        /// // Constructeur(s)

        public NumericTestBean(final Number number) {
            this.number = number;
        }

        /// // Méthode(s)

        @Override
        public int functionnalHashCode() {
            return Objects.hashCode(this.number);
        }

        @Override
        public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
            if (!(obj instanceof NumericTestBean numericTestBean)) {
                return false;
            }
            return Objects.equals(this.number, numericTestBean.number);
        }

        @Override
        public JSONObject toJSON() {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", this.getId());
            jsonObject.put("number", this.number);
            return jsonObject;
        }

        /// // Getters & Setters

        public Number getNumber() {
            return number;
        }

        public void setNumber(Number number) {
            this.number = number;
        }
    }
}