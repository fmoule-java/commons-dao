package org.commons.dao.jdbc.query.condition;

import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.general.SQLCondition;
import org.commons.dao.jdbc.utils.SQLUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.EQUAL;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.SQLOperator.NOT_EQUAL;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.withFieldEqualsTo;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.withFieldNotEqualsTo;

class SQLFieldOperatorConditionTest {

    /// // Tests unitaires :

    @Test
    public void shouldNegateOperator() {
        assertThat(EQUAL.negate()).isEqualTo(NOT_EQUAL);
        assertThat(NOT_EQUAL.negate()).isEqualTo(EQUAL);
    }

    @Test
    public void shouldEscapeCharacters() {
        assertThat(SQLUtils.escapeSpecialCharacters("L'atelier")).isEqualTo("L''atelier");
        assertThat(SQLUtils.escapeSpecialCharacters("otherField")).isEqualTo("otherField");
        assertThat(SQLUtils.escapeSpecialCharacters("")).isEqualTo("");
        assertThat(SQLUtils.escapeSpecialCharacters(null)).isEqualTo(null);
    }

    @Test
    public void shouldGenerateSQLCodeWithNulleValue() {
        assertThat(withFieldEqualsTo("a", "colonne", null).toSQL())
                .isEqualTo("a.colonne IS NULL");
    }

    @Test
    public void shouldGenerateSQLCodeWithValue() {
        assertThat(withFieldEqualsTo("a", "colonne", "strValue").toSQL())
                .isEqualTo("a.colonne = 'strValue'");
    }

    @Test
    public void shouldGenerateSQLCodeWithNumber() {
        assertThat(withFieldEqualsTo("a", "colonne", new BigDecimal("23.45")).toSQL())
                .isEqualTo("a.colonne = 23.45");
    }

    @Test
    public void shouldGenerateSQLCodeWithField() {
        assertThat(withFieldEqualsTo("a", "colonne", new SQLField("b", "colonne2")).toSQL())
                .isEqualTo("a.colonne = b.colonne2");
    }

    @Test
    public void shouldNegate() throws SQLException {
        final SQLCondition negateSQLCondition = withFieldEqualsTo("a", "monChamps", 5).negate();
        assertThat(negateSQLCondition).isEqualTo(withFieldNotEqualsTo("a", "monChamps", 5));
        assertThat(negateSQLCondition.toSQL()).isEqualTo("a.monChamps != 5");
    }
}