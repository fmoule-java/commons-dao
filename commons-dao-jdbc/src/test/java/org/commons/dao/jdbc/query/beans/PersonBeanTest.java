package org.commons.dao.jdbc.query.beans;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.utils.ObjectUtils.cast;

class PersonBeanTest {

    /// // Tests unitaires

    @Test
    public void shouldClone() throws Exception {
        final PersonBean personBean = new PersonBean("frederic", "moule");
        final PersonBean clonedPersnBean  = cast(personBean.clone(), new Exception("Le clone doit avoir la bonne classe"));
        assertThat(clonedPersnBean).isNotNull();
        assertThat(personBean.equals(clonedPersnBean)).isTrue();
        assertThat(personBean != clonedPersnBean).isTrue();
    }
}