package org.commons.dao.jdbc.query.builder;

import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.builder.SQLPreparedStatementQueryBuilder.createSQLreparedStatementBuilder;
import static org.commons.dao.jdbc.query.condition.DirectSQLCondition.withDirectSQLCondition;
import static org.commons.dao.jdbc.query.elements.QueryType.INSERT;
import static org.commons.dao.jdbc.query.elements.QueryType.UPDATE;

class SQLPreparedStatementQueryBuilderTest {

    /// // Tests unitaires

    @Test
    public void shoudGenerateInsertSQLQuery() throws Exception {
        SQLQueryBuilder sqlBuilder = createSQLreparedStatementBuilder(INSERT);
        sqlBuilder = sqlBuilder.setTable(new SQLTable("fredTable"));
        sqlBuilder.addColumn(new SQLField("colonne1"));
        sqlBuilder.addColumn(new SQLField("colonne2"));
        assertThat(sqlBuilder.toSQL()).isEqualTo("INSERT INTO fredTable (colonne1, colonne2) VALUES (?, ?)");
    }

    @Test
    public void shoudGenerateUpdateSQLQuery() throws Exception {
        SQLQueryBuilder sqlBuilder = createSQLreparedStatementBuilder(UPDATE);
        sqlBuilder = sqlBuilder.setTable(new SQLTable("fredTable"));
        sqlBuilder.addColumn(new SQLField("colonne1"));
        sqlBuilder.addColumn(new SQLField("colonne2"));
        sqlBuilder.addWhereCondition(withDirectSQLCondition("id = ?"));
        assertThat(sqlBuilder.toSQL()).isEqualTo("UPDATE fredTable SET colonne1 = ?, colonne2 = ? WHERE id = ?");
    }

}