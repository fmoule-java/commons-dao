package org.commons.dao.jdbc.query;

import org.commons.dao.jdbc.query.elements.SQLField;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class SQLFieldTest {

    /// // Tests unitaires

    @SuppressWarnings("EqualsWithItself")
    @Test
    public void shouldBeEquals() {
        SQLField sqlField = new SQLField("fredField");
        assertThat(sqlField).isEqualTo(new SQLField("fredField"));
        assertThat(sqlField.hashCode()).isEqualTo(new SQLField("fredField").hashCode());
        assertThat(sqlField.compareTo(new SQLField("fredField"))).isEqualTo(0);
        assertThat(sqlField).isNotEqualTo(new SQLField("otherField"));
        assertThat(sqlField).isNotEqualTo(null);
        assertThat(sqlField).isEqualTo(sqlField);
    }

    @Test
    public void shouldThrowExceptionWhenTryingToCopy() {
        try {
            new SQLField((SQLField) null);
            fail("Doit renvoyer une exception");
        } catch (final Exception exception) {
            assertThat(exception instanceof NullPointerException).isTrue();
        }
    }

    @Test
    public void shouldBeConvertedIntoSQLCode() {
        SQLField sqlField = new SQLField("fredField");
        assertThat(sqlField.toSQL()).isEqualTo("fredField");
        sqlField = new SQLField("prefix", "fredField");
        assertThat(sqlField.toSQL()).isEqualTo("prefix.fredField");
    }

}