package org.commons.dao.jdbc.query.builder;

import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.builder.SQLQueryBuilder.createSQLBuilder;
import static org.commons.dao.jdbc.query.condition.SQLFieldOperatorCondition.withFieldEqualsTo;
import static org.commons.dao.jdbc.query.elements.QueryType.*;

class SQLQueryBuilderTest {

    /// // Tests unitaires

    @Test
    public void shouldCreateSimpleSelectQuery() throws SQLException {
        SQLQueryBuilder sqlQueryBuilder = createSQLBuilder(SELECT);
        sqlQueryBuilder = sqlQueryBuilder.setTable(new SQLTable("fredTable", "mainTable"));
        assertThat(sqlQueryBuilder.toSQL()).isEqualTo("SELECT * FROM fredTable AS mainTable");
    }

    @Test
    public void shouldCreateSelectQueryWithColumns() throws SQLException {
        SQLQueryBuilder sqlQueryBuilder = createSQLBuilder(SELECT);
        sqlQueryBuilder = sqlQueryBuilder.addColumn(new SQLField("field1"));
        sqlQueryBuilder = sqlQueryBuilder.addColumn(new SQLField(null, "field2", "alias2"));
        sqlQueryBuilder = sqlQueryBuilder.setTable(new SQLTable("fredTable", "mainTable"));
        assertThat(sqlQueryBuilder.toSQL()).isEqualTo("SELECT field1, field2 AS alias2 FROM fredTable AS mainTable");
    }

    @Test
    public void shouldCreateSelectQueryWithWhere() throws SQLException {
        SQLQueryBuilder sqlQueryBuilder = createSQLBuilder(SELECT);
        sqlQueryBuilder = sqlQueryBuilder.addColumn(new SQLField(null, "field1", "alias1"));
        sqlQueryBuilder = sqlQueryBuilder.addColumn(new SQLField(null, "field2", "alias2"));
        sqlQueryBuilder = sqlQueryBuilder.setTable(new SQLTable("fredTable", "t"));
        sqlQueryBuilder = sqlQueryBuilder.addWhereCondition(withFieldEqualsTo("t", "field2", "strValue"));
        assertThat(sqlQueryBuilder.toSQL()).isEqualTo("SELECT field1 AS alias1, field2 AS alias2 FROM fredTable AS t WHERE t.field2 = 'strValue'");
    }

    @Test
    public void shouldCreateDeleteQuery() throws SQLException {
        SQLQueryBuilder queryBuilder = createSQLBuilder(DELETE);
        queryBuilder = queryBuilder.setTable(new SQLTable("fredTable"));
        queryBuilder = queryBuilder.addWhereCondition(withFieldEqualsTo("t", "field2", "strValue"));
        assertThat(queryBuilder.toSQL()).isEqualTo("DELETE FROM fredTable WHERE t.field2 = 'strValue'");
    }

    @Test
    public void shouldCreateInsertQuery() throws SQLException {
        SQLQueryBuilder sqlBuilder = createSQLBuilder(INSERT);
        sqlBuilder = sqlBuilder.setTable(new SQLTable("person", "p"));
        sqlBuilder.addNewValue("firstName", "gael");
        sqlBuilder.addNewValue("lastName", "lebreton");
        sqlBuilder.addNewValue("id", 5);
        assertThat(sqlBuilder.toSQL()).isEqualTo("INSERT INTO person (firstName, id, lastName) VALUES ('gael', 5, 'lebreton')");
    }

    @Test
    public void shouldCreateInsertQueryWithNull() throws SQLException {
        SQLQueryBuilder sqlBuilder = createSQLBuilder(INSERT);
        sqlBuilder = sqlBuilder.setTable(new SQLTable("person", "p"));
        sqlBuilder.addNewValue("firstName", "gael");
        sqlBuilder.addNewValue("lastName", "lebreton");
        sqlBuilder.addNewValue("id", null);
        assertThat(sqlBuilder.toSQL()).isEqualTo("INSERT INTO person (firstName, id, lastName) VALUES ('gael', NULL, 'lebreton')");
    }

}