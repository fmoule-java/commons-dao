package org.commons.dao.jdbc.query;

import org.commons.dao.jdbc.query.elements.SQLField;
import org.commons.dao.jdbc.query.elements.SQLTable;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SQLTableTest {

    /// // Tests unitaires

    @Test
    public void shouldBeEquals() {
        SQLTable sqlTable = new SQLTable("fredTable");
        assertThat(sqlTable).isEqualTo(new SQLTable("fredTable"));
        assertThat(sqlTable.hashCode()).isEqualTo(new SQLTable("fredTable").hashCode());
        sqlTable = new SQLTable("fredTable", "alias");
        assertThat(sqlTable).isEqualTo(new SQLTable("fredTable", "alias"));
        assertThat(sqlTable.hashCode()).isEqualTo(new SQLTable("fredTable", "alias").hashCode());
        assertThat(sqlTable).isNotEqualTo(new SQLTable("fredTable", "otherAlias"));
        assertThat(sqlTable).isNotEqualTo(new SQLTable("otherTable"));
        assertThat(sqlTable).isNotEqualTo(new SQLField("fredTable"));
        assertThat(sqlTable).isNotEqualTo("toto");
        assertThat(sqlTable).isNotEqualTo(null);
    }

    @Test
    public void shouldConvertIntoSQL() throws Exception {
        SQLTable sqlTable = new SQLTable("fredTable");
        assertThat(sqlTable.toSQL()).isEqualTo("fredTable");
        sqlTable = new SQLTable("fredTable", "aliasTable");
        assertThat(sqlTable.toSQL()).isEqualTo("fredTable AS aliasTable");
    }

}