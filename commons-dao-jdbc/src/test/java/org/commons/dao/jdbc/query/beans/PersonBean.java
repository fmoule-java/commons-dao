package org.commons.dao.jdbc.query.beans;

import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jdbc.beans.AbstractJDBCDAOBean;
import org.json.JSONObject;

import java.util.Objects;

public class PersonBean extends AbstractJDBCDAOBean<Long> {
    private Long id;
    private String firstName;
    private String lastName;
    private CompanyBean company;


    /// // Constructeur(s)

    public PersonBean(final Long id, final String firstName, final String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public PersonBean(final String firstName, final String lastName) {
        this(null, firstName, lastName);
    }

    public PersonBean() {
        this(null, null);
    }

    /// // Méthode(s) de la classe AbstractDAOBean :

    @Override
    public int functionnalHashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof PersonBean personBean)) {
            return false;
        }
        boolean areEquals = Objects.equals(this.firstName, personBean.firstName);
        areEquals = areEquals && Objects.equals(this.lastName, personBean.lastName);
        return areEquals;
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("firstName", firstName);
        jsonObject.put("lastName", lastName);
        return jsonObject;
    }

    /// // Getters & Setters

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }


    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public CompanyBean getCompany() {
        return company;
    }

    public Long getCompanyId() {
        return (company == null ? null : company.getId());
    }

    public void setCompany(final CompanyBean company) {
        this.company = company;
    }
}
