package org.commons.dao.jdbc.query.elements;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jdbc.query.elements.QueryType.*;

class QueryTypeTest {

    /// // Tests unitaires :

    @Test
    public void shouldGetSQLName() {
        assertThat(INSERT.getSQLName()).isEqualTo("INSERT");
        assertThat(UPDATE.getSQLName()).isEqualTo("UPDATE");
        assertThat(SELECT.getSQLName()).isEqualTo("SELECT");
        assertThat(DELETE.getSQLName()).isEqualTo("DELETE");
    }

}