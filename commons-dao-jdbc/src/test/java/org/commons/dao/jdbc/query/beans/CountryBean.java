package org.commons.dao.jdbc.query.beans;

import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Objects;

public class CountryBean extends AbstractDAOBean<Long> {
    private Long id;
    private String name;

    /// // Constructeur(s) :

    public CountryBean(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public CountryBean(final String name) {
        this(null, name);
    }

    public CountryBean() {
        this(null);
    }

    /// // Méthode(s) de la classe AbstractDAOBean :

    @Override
    public int functionnalHashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof CountryBean countryBean)) {
            return false;
        }
        return Objects.equals(name, countryBean.name);
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonCountry = new JSONObject();
        jsonCountry.put("id", id);
        jsonCountry.put("name", name);
        return jsonCountry;
    }

    /// // Getters & Setters

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
