package org.commons.dao.utils.json;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.utils.json.JSONArrayUtils.findJSONObjectIn;

class JSONArrayUtilsTest {

    ///// Tests unitaires :

    @Test
    public void shouldFindJSONObject() {
        final JSONArray jsonArray = new JSONArray("[{\"field1\":\"val1\",\"field2\":\"val2\"},{\"field1\":\"val3\",\"field2\":\"val4\"}]");
        JSONObject foundJsonObject = findJSONObjectIn(jsonArray, (jsonObject -> jsonObject.has("field1") && "val1".equals(jsonObject.getString("field1"))));
        assertThat(foundJsonObject).isNotNull();
        assertThat(foundJsonObject.getString("field2")).isEqualTo("val2");
    }

}