package org.commons.dao.utils.bean;


import java.util.Objects;

public class TestBean {
    private final String name;
    private Long id;

    /// // Constructeur(s)

    public TestBean(final String name) {
        this(null, name);
    }

    public TestBean(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    /// // Méthodes :

    @Override
    public int hashCode() {
        int hashCode = Objects.hashCode(this.id);
        hashCode = (17 * hashCode) + Objects.hashCode(this.name);
        return hashCode;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (this.getClass() != obj.getClass())) {
            return false;
        }
        final TestBean that = (TestBean) obj;
        boolean areEquals = Objects.equals(this.id, that.id);
        areEquals = areEquals && Objects.equals(this.name, that.name);
        return areEquals;
    }

    /// // Getters & Setters :

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
}
