package org.commons.dao.utils.json;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.utils.json.JSONUtils.convertToMap;

class JSONUtilsTest {

    ///// Tests unitaires :

    @Test
    public void shouldConvertIntoMap() {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("prop1", "val1");
        jsonObject.put("prop2", "val2");
        final Map<String, Object> theMap = convertToMap(jsonObject);
        assertThat(theMap).isNotNull();
        assertThat(theMap.size()).isEqualTo(2);
        assertThat(theMap.get("prop1")).isEqualTo("val1");
        assertThat(theMap.get("prop2")).isEqualTo("val2");
    }

    @Test
    public void shouldConvertIntoJSONCode() throws Exception {
        final TestBean testBean = new TestBean("prop", "value");
        assertThat(JSONUtils.toJSONCode(testBean)).isEqualTo("{\"name\":\"prop\",\"value\":\"value\"}");
    }


    private static class TestBean implements ToJSON {
        private String name;
        private String value;

        public TestBean(final String name, final String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public JSONObject toJSON() {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", this.name);
            jsonObject.put("value", this.value);
            return jsonObject;
        }
    }

}