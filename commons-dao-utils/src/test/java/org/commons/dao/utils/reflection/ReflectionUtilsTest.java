package org.commons.dao.utils.reflection;

import org.commons.dao.utils.bean.TestBean;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.utils.reflection.ReflectionUtils.*;

class ReflectionUtilsTest {

    /// // Tests unitaires :

    @Test
    public void shouldBuildGetterName() throws Exception {
        assertThat(buildGetterNameFromFieldName("company_id")).isEqualTo("getCompanyId");
        assertThat(buildGetterNameFromFieldName("get_company_id")).isEqualTo("getCompanyId");
        assertThat(buildGetterNameFromFieldName("company")).isEqualTo("getCompany");
    }

    @Test
    public void shouldListTheGetters() {
        final Set<Method> getters = listGetters(TestBean.class);
        assertThat(getters).isNotNull();
        assertThat(getters).isNotEmpty();
        assertThat(getters.size()).isEqualTo(3);
    }

    @Test
    public void shouldTranscodePackageName() {
        assertThat("org.commons.dao.utils".replaceAll("[.]", "/"))
                .isEqualTo("org/commons/dao/utils");
    }

    @Test
    public void shouldGetClassFromPackage() {
        Optional<Class<?>> classOptional = getClassInPackage("org.commons.dao.utils.bean", "TestBean");
        assertThat(classOptional.isPresent()).isTrue();
        assertThat(classOptional.get()).isEqualTo(TestBean.class);
        classOptional = getClassInPackage("org.commons.dao.utils.bean", "TestBean.class");
        assertThat(classOptional.isPresent()).isTrue();
        assertThat(classOptional.get()).isEqualTo(TestBean.class);
        classOptional = getClassInPackage("org.commons.dao.utils.bean", "NotExistingBean.class");
        assertThat(classOptional.isPresent()).isFalse();
        classOptional = getClassInPackage("org.commons.dao.utils.bean", "package");
        assertThat(classOptional.isEmpty()).isTrue();
    }

    @Test
    public void shouldClassesFromPackage() throws Exception {
        Set<Class<?>> classes = listPackageClasses("org.commons.dao.utils.bean");
        assertThat(classes).isNotNull();
        assertThat(classes).isNotEmpty();
        assertThat(classes.size()).isEqualTo(1);
        assertThat(classes.contains(TestBean.class)).isTrue();

        classes = listPackageClasses("org.commons.dao.utils");
        assertThat(classes).isNotNull();
        assertThat(classes).isNotEmpty();
    }

    @Test
    public void shouldGetTheGetter() throws Exception {
        Method getterMethod = getGetter(TestBean.class, "id");
        assertThat(getterMethod).isNotNull();
        assertThat(getterMethod.invoke(new TestBean(13L, "bean"))).isEqualTo(13L);
        assertThat(getterMethod.invoke(new TestBean(null, "bean"))).isNull();
        getterMethod = getGetter(TestBean.class, "name");
        assertThat(getterMethod.invoke(new TestBean(null, "bean"))).isEqualTo("bean");
    }

}