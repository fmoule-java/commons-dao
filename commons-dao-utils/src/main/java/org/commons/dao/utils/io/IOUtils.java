package org.commons.dao.utils.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;

public class IOUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(IOUtils.class);

    private IOUtils() {
        // EMPTY
    }

    /// // Méthode(s) statique(s)

    public static void closeQuietly(final Closeable closeable) {
        org.apache.commons.io.IOUtils.closeQuietly(closeable);
    }

    public static void closeQuietly(final AutoCloseable autoCloseable) {
        if (autoCloseable == null) {
            return;
        }
        try {
            autoCloseable.close();
        } catch (final Exception exception) {
            LOGGER.warn(exception.getMessage(), exception);
        }
    }
}
