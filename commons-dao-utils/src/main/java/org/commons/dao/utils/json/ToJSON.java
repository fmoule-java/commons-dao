package org.commons.dao.utils.json;

import org.json.JSONObject;

/**
 * <p>Interface décrivant tous les objets convertibles en code JSON</p>
 */
public interface ToJSON {

    /**
     * Méthode permettant de transformer l'instance courante en objet JSONObject ou JSONArray. <br />
     *
     * @return objet JSON
     */
    JSONObject toJSON();

    /**
     * Retourne le code JSON en tant que chaîne de caractères. <br />
     *
     * @return code JSON
     */
    default String toJSONAsString() {
        return toJSON().toString();
    }

}
