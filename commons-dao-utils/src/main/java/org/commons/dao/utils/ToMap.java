package org.commons.dao.utils;

import java.util.Map;

public interface ToMap {

    /**
     * Méthode permettant de transformer l'instance courante
     * en instance de Map<String, Object><br />
     *
     * @return instance de Map
     */
    Map<String, Object> toMap();
}
