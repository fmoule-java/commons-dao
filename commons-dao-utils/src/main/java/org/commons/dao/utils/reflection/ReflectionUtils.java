package org.commons.dao.utils.reflection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Predicate;

import static java.lang.ClassLoader.getSystemClassLoader;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * <p>
 * Classe utilitaire regroupant toutes les méthodes utiles et utilisant la réflexivité. <br />
 * </p>
 */
public class ReflectionUtils {
    private static final GetterPredicate GETTER_PREDICATE = new GetterPredicate();
    private static final String GET_PREFIX = "get";

    private ReflectionUtils() {
        // EMPTY
    }

    public static Set<Method> listMethods(final Class<?> clazz, final Predicate<Method> methodNamePredicate) {
        final Set<Method> methods = new HashSet<>();
        if (clazz == null) {
            return methods;
        }
        final Map<String, Method> methodMap = new HashMap<>();
        for (Method method : clazz.getMethods()) {
            if (!methodNamePredicate.test(method)) {
                continue;
            }
            methodMap.put(method.getName(), method);
        }
        methods.addAll(methodMap.values());
        return methods;
    }

    /**
     * Méthode permettant de lister les "getters" de la
     * classe passée en paramètre de la méthode. <br />
     *
     * @param clazz : classe
     * @return liste des "getters"
     */
    public static Set<Method> listGetters(final Class<?> clazz) {
        return listMethods(clazz, GETTER_PREDICATE);
    }

    /**
     * Méthode permettant de lister les "getters" de la
     * classe passée en paramètre de la méthode. <br />
     *
     * @param clazz : classe
     * @return liste des "getters"
     */
    public static Set<Method> listGetters(final Class<?> clazz, final Predicate<Method> predicate) {
        Predicate<Method> methodPredicate = GETTER_PREDICATE;
        methodPredicate = methodPredicate.and(predicate);
        return listMethods(clazz, methodPredicate);
    }

    /**
     * Retourne le getter correspondant au nom du champs. <br />
     *
     * @param clazz     : classe concerné
     * @param fieldName : nom du champs
     * @return instance de Method correspondant au getter
     * @see Method
     */
    public static Method getGetter(final Class<?> clazz, final String fieldName) {
        if (clazz == null || isEmpty(fieldName)) {
            return null;
        }
        try {
            return clazz.getMethod(buildGetterNameFromFieldName(fieldName));
        } catch (final NoSuchMethodException e) {
            return null;
        }
    }

    public static String buildGetterNameFromFieldName(final String fieldName) {
        final StringTokenizer tokenizer = new StringTokenizer(fieldName, "_", false);
        final StringBuilder buffer = new StringBuilder();
        int cursor = 0;
        String token;
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();
            if (cursor == 0 && !token.startsWith(GET_PREFIX)) {
                buffer.append(GET_PREFIX);
                buffer.append(token.substring(0, 1).toUpperCase());
                buffer.append(token.substring(1));
            } else if (cursor == 0) {
                buffer.append(token);
            } else {
                buffer.append(token.substring(0, 1).toUpperCase());
                buffer.append(token.substring(1));
            }
            cursor++;
        }
        return buffer.toString();
    }

    /**
     * <p>
     * Permet de récupérer une classe en fonction de son package et de son nom. <br />
     * </p>
     *
     * @param packageName : nom du package
     * @param className   : nom de la classe
     * @return classe sous forme d'une instance Optional
     */
    public static Optional<Class<?>> getClassInPackage(final String packageName, final String className) {
        if (isEmpty(packageName) || isEmpty(className)) {
            return Optional.empty();
        }
        try {
            int lastDotIndex = className.lastIndexOf('.');
            String theClassParameter;
            if (lastDotIndex == -1) {
                theClassParameter = className;
            } else {
                theClassParameter = className.substring(0, lastDotIndex);
            }
            return Optional.of(Class.forName(packageName + "." + theClassParameter));
        } catch (final ClassNotFoundException classNotFoundException) {
            return Optional.empty();
        }
    }

    /**
     * Méthode permettant de lister toutes les classes se trouvant
     * dans le package et les sous-packages représentés par la chaîne
     * de caractères passée en paramètre.
     *
     * @param packageName : nom du package
     * @return collection des classes.
     */
    public static Set<Class<?>> listPackageClasses(final String packageName) throws IOException {
        final Set<Class<?>> classes = new HashSet<>();
        if (isEmpty(packageName)) {
            return classes;
        }
        final List<String> packageToExplore = new ArrayList<>();
        packageToExplore.add(packageName);
        String thePackage;
        InputStream stream;
        BufferedReader bufferedReader;
        String line;
        Optional<Class<?>> classOpt;
        while (!packageToExplore.isEmpty()) {
            stream = null;
            bufferedReader = null;
            thePackage = packageToExplore.removeFirst();
            try {
                stream = getSystemClassLoader().getResourceAsStream(thePackage.replaceAll("[.]", "/"));
                if (stream == null) {
                    continue;
                }
                bufferedReader = new BufferedReader(new InputStreamReader(stream));
                while ((line = bufferedReader.readLine()) != null) {
                    if (!line.endsWith(".class")) {
                        packageToExplore.add(thePackage + "." + line);
                        continue;
                    }
                    classOpt = getClassInPackage(thePackage, line);
                    if (classOpt.isEmpty()) {
                        continue;
                    }
                    classes.add(classOpt.get());
                }
            } finally {
                closeQuietly(bufferedReader, stream);
            }
        }
        return classes;
    }


    /// // Classe(s) interne(s) :

    public static class GetterPredicate implements Predicate<Method> {

        @Override
        public boolean test(final Method method) {
            if (method == null) {
                return false;
            }
            return method.getName().startsWith(GET_PREFIX);
        }
    }


}
