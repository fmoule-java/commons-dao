package org.commons.dao.utils.json;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe utilitaire regroupant toutes les fonctions
 * concernant les objets JSON. <br />
 *
 * @see JSONObject
 */
public class JSONUtils {
    private static final String EMPTY_JSON_CODE = "{}";

    private JSONUtils() {
        // EMPTY
    }

    /**
     * Méthode permettant de convertir une instance de JSONObject
     * en dictionnaire
     *
     * @param jsonObject : object JSON à convertir
     * @return dictionnaire
     */
    public static Map<String, Object> convertToMap(final JSONObject jsonObject) {
        final Map<String, Object> properties = new HashMap<>();
        if (jsonObject == null) {
            return properties;
        }
        for (String key : jsonObject.keySet()) {
            properties.put(key, jsonObject.get(key));
        }
        return properties;
    }

    /**
     * Méthode permettant de construire un objet JSON
     * à partir d'une Map. <br />
     *
     * @param parameters : map des paramètres
     * @return instance de JSONObject
     * @see JSONObject
     * @see Map
     */
    public static JSONObject buildFromMap(final Map<String, Object> parameters) {
        final JSONObject jsonObject = new JSONObject();
        if (parameters == null || parameters.isEmpty()) {
            return jsonObject;
        }
        for (String paramName : parameters.keySet()) {
            jsonObject.put(paramName, parameters.get(paramName));
        }
        return jsonObject;
    }

    public static String toJSONCode(final ToJSON bean) {
        if (bean == null) {
            return EMPTY_JSON_CODE;
        }
        final JSONObject jsonBean = bean.toJSON();
        if (jsonBean == null) {
            return EMPTY_JSON_CODE;
        }
        return jsonBean.toString();
    }

}
