package org.commons.dao.utils.json;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import static org.apache.commons.lang3.StringUtils.isEmpty;


public class JSONArrayUtils {

    private JSONArrayUtils() {
        // EMPTY
    }

    public static boolean isJSONArrayEmpty(JSONArray jsonArray) {
        return jsonArray == null || jsonArray.length() == 0;
    }

    public static <T> Set<T> filterJSONArray(final JSONArray jsonArray, final Predicate<T> predicate) {
        final Set<T> hashSet = new HashSet<>();
        T bean;
        for (Object item : jsonArray) {
            bean = (T) item;
            if (predicate.test(bean)) {
                hashSet.add(bean);
            }
        }
        return hashSet;
    }

    public static JSONObject findJSONObjectIn(final JSONArray jsonArray, final Predicate<JSONObject> predicate) {
        if (isJSONArrayEmpty(jsonArray)) {
            return null;
        }
        Object item;
        for (int i = 0; i < jsonArray.length(); i++) {
            item = jsonArray.get(i);
            if (!(item instanceof JSONObject)) {
                continue;
            }
            if (predicate.test((JSONObject) item)) {
                return (JSONObject) item;
            }
        }
        return null;
    }

    public static JSONArray toJSONArray(final Collection<? extends ToJSON> items) {
        final JSONArray jsonArray = new JSONArray();
        if (items == null || items.isEmpty()) {
            return jsonArray;
        }
        for (ToJSON item : items) {
            jsonArray.put(item.toJSON());
        }
        return jsonArray;
    }

    /**
     * <p>
     * Teste si un code JSON est contenu dans une instance de JSONArray. <br />
     * </p>
     *
     * @param jsonArray : JSON array
     * @param jsonCode  : code JSON
     * @return booléen montrant si le code JSON est contenu ou non.
     */
    public static boolean doesJSONArrayContain(final JSONArray jsonArray, final String jsonCode) {
        if (isEmpty(jsonCode) || (jsonArray == null) || (jsonArray.isEmpty())) {
            return false;
        }
        return jsonArray.toString().contains(jsonCode);
    }
}
