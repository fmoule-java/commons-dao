package org.commons.dao.collection;

import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

class ListDAOTest {
    private final ListDAO<StringBean> listDAO = new ListDAO<>();

    @BeforeEach
    void setUp() {
        listDAO.clear();
    }

    ///// Tests unitaires :

    @Test
    public void shouldSelect() throws Exception {
        listDAO.addAll(asList(new StringBean("item1"), new StringBean("item2"), new StringBean("item1")));
        final List<StringBean> foundItems = listDAO.selectAll();
        assertThat(foundItems).isNotEmpty();
        assertThat(foundItems.size()).isEqualTo(3);
        assertThat(foundItems.get(0)).isEqualTo(new StringBean("item1"));
        assertThat(foundItems.get(1)).isEqualTo(new StringBean("item2"));
        assertThat(foundItems.get(2)).isEqualTo(new StringBean("item1"));
    }

    @Test
    public void shouldSelectWithPredicate() {
        listDAO.addAll(asList(new StringBean("item1"), new StringBean("fred"), new StringBean("item2"), new StringBean("gael")));
        final List<StringBean> foundItems = listDAO.select(strBean -> strBean != null && strBean.getStringValue().startsWith("item"));
        assertThat(foundItems).isNotEmpty();
        assertThat(foundItems.size()).isEqualTo(2);
        assertThat(foundItems.get(0)).isEqualTo(new StringBean("item1"));
        assertThat(foundItems.get(1)).isEqualTo(new StringBean("item2"));
    }

    @Test
    public void shouldDeleteItems() throws Exception {
        listDAO.addAll(asList(new StringBean("item1"), new StringBean("item2"), new StringBean("item2"), new StringBean("item3")));
        listDAO.delete(strBean -> "item2".equals(strBean.getStringValue()));
        final List<StringBean> allItems = listDAO.selectAll();
        assertThat(allItems).isNotEmpty();
        assertThat(allItems.size()).isEqualTo(2);
        assertThat(allItems.get(0)).isEqualTo(new StringBean("item1"));
        assertThat(allItems.get(1)).isEqualTo(new StringBean("item3"));
    }


    private static class StringBean extends AbstractDAOBean<String> {
        private String str;

        public StringBean(final String str) {
            this.str = str;
        }

        @Override
        public int functionnalHashCode() {
            return Objects.hashCode(this.str);
        }

        @Override
        public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
            if (!(obj instanceof StringBean stringBean)) {
                return false;
            }
            return Objects.equals(this.str, stringBean.str);
        }

        @Override
        public JSONObject toJSON() {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("value", str);
            return jsonObject;
        }

        /// // Getters & Setters

        @Override
        public String getId() {
            return this.str;
        }

        @Override
        public void setId(final String id) {
            this.str = id;
        }

        public String getStringValue() {
            return (this.str == null ? "" : this.str);
        }
    }
}