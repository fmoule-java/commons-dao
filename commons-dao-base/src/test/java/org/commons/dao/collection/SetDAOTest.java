package org.commons.dao.collection;

import org.apache.commons.lang3.tuple.Pair;
import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static java.util.Arrays.asList;
import static org.apache.commons.collections4.ComparatorUtils.naturalComparator;
import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.base.DAO.fetchFirstElementFromDAO;

public class SetDAOTest {
    private SetDAO<Person> personDAO;

    @BeforeEach
    void setUp() {
        personDAO = new SetDAO<>();
    }

    @AfterEach
    void tearDown() {
        personDAO.close();
    }

    ///// Tests unitaires :


    @Test
    public void shouldReturnTheNumberOfElements() throws Exception {
        personDAO.save(new Person("frederic", "moule"));
        personDAO.save(new Person("augustin", "dubuc"));
        assertThat(personDAO.selectCount()).isEqualTo(2L);
        personDAO.clear();
        assertThat(personDAO.selectCount()).isEqualTo(0L);
    }

    @Test
    public void shouldReturnSortedBeans() throws Exception {
        personDAO.addAll(asList(new Person("fred", "moule"), new Person("gael", "lebreton"), new Person("cyril", "courtonne")));
        final List<Person> persons = new ArrayList<>(personDAO.selectAll(new PersonComparator()));
        assertThat(persons).isNotEmpty();
        assertThat(persons.get(0)).isEqualTo(new Person("cyril", "courtonne"));
        assertThat(persons.get(1)).isEqualTo(new Person("fred", "moule"));
        assertThat(persons.get(2)).isEqualTo(new Person("gael", "lebreton"));
    }

    @Test
    public void shouldDeleteAll() throws Exception {
        personDAO.addAll(asList(new Person("fred", "moule"), new Person("gael", "lebreton"), new Person("cyril", "courtonne")));
        assertThat(personDAO.selectAll().size()).isEqualTo(3);
        personDAO.deleteAll();
        assertThat(personDAO.selectAll().size()).isEqualTo(0);
    }


    @Test
    public void shouldDeleteABean() throws Exception {
        personDAO.addAll(asList(new Person("fred", "moule"), new Person("gael", "lebreton"), new Person("cyril", "courtonne")));
        assertThat(personDAO.selectAll().size()).isEqualTo(3);
        personDAO.delete(Collections.singleton(new Person("fred", "moule")));
        final List<Person> beans = personDAO.selectAll();
        assertThat(beans.size()).isEqualTo(2);
        assertThat(beans.contains(new Person("fred", "moule"))).isFalse();
    }


    @Test
    public void shouldSave() throws Exception {
        assertThat(personDAO.selectAll()).isEmpty();
        personDAO.save(new Person("fred", "moule"));
        assertThat(personDAO.selectAll().size()).isEqualTo(1);
    }


    @Test
    public void shouldSaveACollection() throws Exception {
        assertThat(personDAO.selectAll()).isEmpty();
        personDAO.save(asList(new Person("fred", "moule"), new Person("gael", "lebreton")));
        final List<Person> savedBeans = personDAO.selectAll();
        assertThat(savedBeans).isNotEmpty();
        assertThat(savedBeans.size()).isEqualTo(2);
        assertThat(savedBeans.contains(new Person("fred", "moule"))).isTrue();
        assertThat(savedBeans.contains(new Person("gael", "lebreton"))).isTrue();
    }


    @Test
    public void shouldBuildFromCollection() throws Exception {
        personDAO = new SetDAO<>(asList(new Person("fred", "moulé"), new Person("gael", "lebreton")));
        final List<Person> people = personDAO.selectAll();
        assertThat(people).isNotEmpty();
        assertThat(people.size()).isEqualTo(2);
        assertThat(people.get(0)).isEqualTo(new Person("fred", "moulé"));
        assertThat(people.get(1)).isEqualTo(new Person("gael", "lebreton"));
    }


    @Test
    public void shouldFetchByPredicat() throws Exception {
        final SetDAO<Person> personDAO = new SetDAO<>();
        personDAO.addAll(asList(new Person("frederic", "moule"), new Person("gael", "lebreton"), new Person("dorine", "leleu")));
        final Optional<Person> foundPersonOpt = fetchFirstElementFromDAO(personDAO, (person -> person.getFirstName() != null && person.getFirstName().startsWith("fred")));
        assertThat(foundPersonOpt).isNotEmpty();
        assertThat(foundPersonOpt.get()).isEqualTo(new Person("frederic", "moule"));
    }


    @Test
    public void shouldGetAllElements() {
        personDAO = new SetDAO<>(asList(new Person("fred", "moulé"), new Person("gael", "lebreton")));
        final Set<Person> elements = (Set<Person>) personDAO.getBeans();
        assertThat(elements).isNotEmpty();
        assertThat(elements.size()).isEqualTo(2);
        assertThat(elements.contains(new Person("fred", "moulé"))).isTrue();
        assertThat(elements.contains(new Person("gael", "lebreton"))).isTrue();
    }

    @Test
    public void shouldGetFirstElementFromComparator() throws Exception {
        personDAO = new SetDAO<>(asList(new Person("fred", "moulé"), new Person("gael", "lebreton"), new Person("cyril", "courtonne")));
        final Optional<Person> firstPersonOpt = personDAO.selectFirst(pers -> (pers != null) && !(pers.firstName.startsWith("f")), new PersonComparator());
        assertThat(firstPersonOpt).isNotEmpty();
        assertThat(firstPersonOpt.get()).isEqualTo(new Person("cyril", "courtonne"));
    }

    ///// Classes internes :

    private static class PersonComparator implements Comparator<Person> {

        @Override
        public int compare(final Person person1, final Person person2) {
            if (person1 == null && person2 == null) {
                return 0;
            } else if (person1 == null) {
                return -1;
            } else if (person2 == null) {
                return 1;
            }
            final Comparator<String> stringComparator = naturalComparator();
            int compResult = stringComparator.compare(person1.getFirstName(), person2.getFirstName());
            if (compResult == 0) {
                return stringComparator.compare(person1.getLastName(), person2.getLastName());
            } else {
                return compResult;
            }
        }
    }

    /**
     * Bean utilisé pour les tests unitaires. <br />
     */
    private static class Person extends AbstractDAOBean<Pair<String, String>> {
        private String firstName;
        private String lastName;

        /// // Constructeur(s) :

        public Person(final String firstName, final String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        /// // Méthodes

        @Override
        public int functionnalHashCode() {
            return Objects.hash(Pair.of(firstName, lastName));
        }

        @Override
        public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
            if (!(obj instanceof Person person)) {
                return false;
            }
            boolean areEquals = Objects.equals(this.firstName, person.firstName);
            areEquals = areEquals && Objects.equals(this.lastName, person.lastName);
            return areEquals;
        }

        @Override
        public String toString() {
            return this.firstName + "-" + this.getLastName();
        }

        @Override
        public JSONObject toJSON() {
            final JSONObject jsonPerson = new JSONObject();
            jsonPerson.put("firstName", firstName);
            jsonPerson.put("lastName", lastName);
            return jsonPerson;
        }

        ///// Getters & Setters :

        @Override
        public Pair<String, String> getId() {
            return Pair.of(this.getFirstName(), this.getLastName());
        }

        @Override
        public void setId(final Pair<String, String> pair) {
            this.firstName = pair.getLeft();
            this.lastName = pair.getRight();
        }

        public String getFirstName() {
            return (firstName == null ? null : this.firstName.trim());
        }

        public String getLastName() {
            return (lastName == null ? null : this.firstName.trim());
        }
    }

}