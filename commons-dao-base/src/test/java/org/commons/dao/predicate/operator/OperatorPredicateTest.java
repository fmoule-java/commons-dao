package org.commons.dao.predicate.operator;

import org.commons.dao.utils.bean.TestBean;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.AND;
import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.OR;
import static org.commons.dao.predicate.operator.OperatorPredicate.and;
import static org.commons.dao.predicate.operator.OperatorPredicate.or;

public class OperatorPredicateTest {

    /// // Tests unitaires

    @Test
    public void shouldTestWithOnePredicate() {
        final Predicate<TestBean> pred1 = (testBean -> testBean != null && testBean.getId() == 15L);
        Predicate<TestBean> predicate = new OperatorPredicate<>(AND, List.of(pred1));
        assertThat(predicate.test(new TestBean(15L, "name"))).isTrue();
        assertThat(predicate.test(new TestBean(15L, "name2"))).isTrue();
        assertThat(predicate.test(new TestBean(145L, "name"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "otherName"))).isFalse();
        assertThat(predicate.test(null)).isFalse();
        predicate = new OperatorPredicate<>(OR, List.of(pred1));
        assertThat(predicate.test(new TestBean(15L, "name"))).isTrue();
        assertThat(predicate.test(new TestBean(15L, "name2"))).isTrue();
        assertThat(predicate.test(new TestBean(145L, "name"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "otherName"))).isFalse();
        assertThat(predicate.test(null)).isFalse();
    }


    @Test
    public void shouldAndPredicateWithOperator() {
        final Predicate<TestBean> predicate = and((testBean -> testBean != null && testBean.getId() == 15L),
                (testBean1 -> testBean1 != null && "name".equals(testBean1.getName())));
        assertThat(predicate.test(new TestBean(15L, "name"))).isTrue();
        assertThat(predicate.test(new TestBean(15L, "name2"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "name"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "otherName"))).isFalse();
        assertThat(predicate.test(null)).isFalse();
    }

    @Test
    public void shouldOrPredicateWithOperator() {
        final Predicate<TestBean> predicate = or((testBean -> testBean != null && testBean.getId() == 15L),
                (testBean1 -> testBean1 != null && "name".equals(testBean1.getName())));
        assertThat(predicate.test(new TestBean(15L, "name"))).isTrue();
        assertThat(predicate.test(new TestBean(15L, "name2"))).isTrue();
        assertThat(predicate.test(new TestBean(145L, "name"))).isTrue();
        assertThat(predicate.test(new TestBean(145L, "otherName"))).isFalse();
        assertThat(predicate.test(null)).isFalse();
    }

    @Test
    public void shouldNegate() {
        final Predicate<TestBean> predicate = or(testBean -> testBean != null && testBean.getId() == 15L,
                (Predicate<TestBean>)(testBean1 -> testBean1 != null && "name".equals(testBean1.getName()))).negate();
        assertThat(predicate.test(new TestBean(15L, "name"))).isFalse();
        assertThat(predicate.test(new TestBean(15L, "name2"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "name"))).isFalse();
        assertThat(predicate.test(new TestBean(145L, "otherName"))).isTrue();
        assertThat(predicate.test(null)).isTrue();
    }

}