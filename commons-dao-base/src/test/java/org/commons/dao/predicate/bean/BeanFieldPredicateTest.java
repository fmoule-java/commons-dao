package org.commons.dao.predicate.bean;

import org.commons.dao.TestBean;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.predicate.bean.BeanFieldPredicate.withBeanFieldEqualsTo;

class BeanFieldPredicateTest {

    /// // Tests unitaires :

    @Test
    public void shouldPredicateTest() {
        assertThat(withBeanFieldEqualsTo("id", 12L)
                .test(new TestBean(14L, "bean"))).isFalse();
        assertThat(withBeanFieldEqualsTo("id", 12L)
                .test(new TestBean(12L, "otherBean"))).isTrue();
        assertThat(withBeanFieldEqualsTo("id", 12L)
                .test(null)).isFalse();
    }

    @Test
    public void shouldTestWithComposite() {
        Predicate<TestCompositeBean> predicate = withBeanFieldEqualsTo("bean.id", 145L);
        assertThat(predicate.test(new TestCompositeBean(145L, "name1", "description1"))).isTrue();
        assertThat(predicate.test(new TestCompositeBean(14L, "name2", "description2"))).isFalse();
        assertThat(predicate.test(null)).isFalse();
        predicate = withBeanFieldEqualsTo("bean.name", "name2");
        assertThat(predicate.test(new TestCompositeBean(145L, "name1", "description1"))).isFalse();
        assertThat(predicate.test(new TestCompositeBean(14L, "name2", "description2"))).isTrue();
        predicate = withBeanFieldEqualsTo("bean", new TestBean(150L, "name"));
        assertThat(predicate.test(new TestCompositeBean(150L, "name", "description"))).isTrue();
        assertThat(predicate.test(new TestCompositeBean(14L, "name", "description"))).isFalse();
    }

    /// // Classe(s) interne(s) :

    @SuppressWarnings("unused")
    private static class TestCompositeBean {
        private final TestBean testBean;
        private final String description;

        public TestCompositeBean(final Long id, String name, String description) {
            this.testBean = new TestBean(id, name);
            this.description = description;
        }

        /// // Getters :

        public TestBean getBean() {
            return testBean;
        }

        public String getDescription() {
            return description;
        }
    }

}