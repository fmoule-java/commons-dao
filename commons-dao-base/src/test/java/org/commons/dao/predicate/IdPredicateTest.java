package org.commons.dao.predicate;

import org.commons.dao.TestBean;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IdPredicateTest {

    ///// Tests unitaires :

    @Test
    public void shouldTestTheId() {
        final IdPredicate<Long, TestBean> predicate = new IdPredicate<>(12L);
        assertThat(predicate.test(new TestBean(12L, "bean"))).isTrue();
        assertThat(predicate.test(new TestBean(10L, "bean"))).isFalse();
    }

    @Test
    public void shouldTestWithNullId() {
        final IdPredicate<Long, TestBean> predicate = new IdPredicate<>(null);
        assertThat(predicate.test(new TestBean(12L, "bean"))).isFalse();
        assertThat(predicate.test(new TestBean(10L, "bean"))).isFalse();
        assertThat(predicate.test(new TestBean(null, "bean"))).isTrue();
    }


    @Test
    public void shouldHandleTheLimitCases() {
        final IdPredicate<Long, TestBean> predicate = new IdPredicate<>(12L);
        assertThat(predicate.test(null)).isFalse();
    }

}