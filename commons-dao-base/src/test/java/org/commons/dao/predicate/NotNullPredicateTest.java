package org.commons.dao.predicate;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NotNullPredicateTest {

    ///// Tests unitaires :

    @Test
    public void shouldTestIfNull() {
        final NotNullPredicate<String> predicate = new NotNullPredicate<>();
        assertThat(predicate.test("test")).isTrue();
        assertThat(predicate.test(null)).isFalse();
    }

}