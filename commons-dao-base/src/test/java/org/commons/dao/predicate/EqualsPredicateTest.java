package org.commons.dao.predicate;

import org.assertj.core.api.Assertions;
import org.commons.dao.TestBean;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EqualsPredicateTest {

    ///// Tests unitaires :

    @Test
    public void shouldTestIfEquals() {
        final EqualsPredicate<TestBean> predicate = new EqualsPredicate<>(new TestBean("bean"));
        assertThat(predicate.test(new TestBean("bean"))).isTrue();
        assertThat(predicate.test(new TestBean("otherValue"))).isFalse();
    }


    @Test
    public void shouldTestInLimitCases() {
        final EqualsPredicate<TestBean> predicate = new EqualsPredicate<>(new TestBean("bean"));
        assertThat(predicate.test(null)).isFalse();
    }


    @Test
    public void shouldTestWithNullBean() {
        final EqualsPredicate<TestBean> predicate = new EqualsPredicate<>(null);
        Assertions.assertThat(predicate.test(null)).isTrue();
        Assertions.assertThat(predicate.test(new TestBean("fred"))).isFalse();
    }


}