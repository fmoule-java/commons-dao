package org.commons.dao.bean;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

class AbstractDAOBeanTest {

    /// // Tests unitaires :

    @Test
    public void shouldTestIfEqualsAndComputeHashcode() {
        assertThat(new TestBean(13L, "name1")).isEqualTo(new TestBean(13L, "name1"));
        assertThat(new TestBean(13L, "name1").hashCode())
                .isEqualTo(new TestBean(13L, "name1").hashCode());
        assertThat(new TestBean(13L, "name1").hashCode())
                .isEqualTo(new TestBean(13L, "otherName").hashCode());
        assertThat(new TestBean(13L, "name1")).isNotEqualTo(new TestBean(145L, "name1"));
        assertThat(new TestBean(13L, "name1")).isNotEqualTo(new TestBean(null, "name1"));
        assertThat(new TestBean( "name1").hashCode())
                .isEqualTo(new TestBean( "name1").hashCode());
        assertThat(new TestBean( "name1")).isNotEqualTo(new TestBean( "otherName"));
    }

    @Test
    public void shouldNotBeEquals() {
        assertThat(new TestBean(null, "name1")).isNotEqualTo(new TestBean(13L, "name1"));
        assertThat(new TestBean(13L, "name1")).isNotEqualTo(new TestBean(null, "name1"));
    }

    /// // Classe(s) interne(s) :

    private static class TestBean extends AbstractDAOBean<Long> {
        private Long id;
        private String name;

        /// // Constructeur(s) :

        public TestBean(final Long id, final String name) {
            this.id = id;
            this.name = name;
        }

        public TestBean(final String name) {
            this(null, name);
        }

        @Override
        public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
            if (!(obj instanceof TestBean)) {
                return false;
            }
            return Objects.equals(name, ((TestBean) obj).name);
        }

        @Override
        public int functionnalHashCode() {
            return Objects.hash(this.name);
        }

        @Override
        public JSONObject toJSON() {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            return jsonObject;
        }

        ///  Getters & Setters

        @Override
        public Long getId() {
            return this.id;
        }

        @Override
        public void setId(final Long id) {
            this.id = id;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}