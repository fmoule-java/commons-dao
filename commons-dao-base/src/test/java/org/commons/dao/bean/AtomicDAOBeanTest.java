package org.commons.dao.bean;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AtomicDAOBeanTest {

    /// // Tests unitaires :

    @Test
    public void shouldBeEquals() {
        assertThat(new AtomicDAOBean<>("testValue")).isEqualTo(new AtomicDAOBean<>("testValue"));
        assertThat(new AtomicDAOBean<>("testValue").hashCode())
                .isEqualTo(new AtomicDAOBean<>("testValue").hashCode());
        assertThat(new AtomicDAOBean<>("testValue")).isNotEqualTo(new AtomicDAOBean<>("otherValue"));
    }

}