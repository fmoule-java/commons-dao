package org.commons.dao;

import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Objects;

public class TestBean extends AbstractDAOBean<Long> {
    private final String name;
    private Long id;

    public TestBean(final String name) {
        this(null, name);
    }

    public TestBean(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    ///// Méthodes :

    @Override
    public int functionnalHashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof TestBean that)) {
            return false;
        }
        return Objects.equals(name, that.name);
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonTestBean = new JSONObject();
        jsonTestBean.put("id", id);
        jsonTestBean.put("name", name);
        return jsonTestBean;
    }

    ///// Getters & Setters :

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
}
