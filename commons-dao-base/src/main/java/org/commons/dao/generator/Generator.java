package org.commons.dao.generator;

public interface Generator<V> {

    /**
     * Fonction permettant de générer les valeurs
     *
     * @return valeur générée
     */
    V generate();
}
