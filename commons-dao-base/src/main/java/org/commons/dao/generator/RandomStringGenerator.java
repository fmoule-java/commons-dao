package org.commons.dao.generator;

import static org.apache.commons.lang3.RandomStringUtils.random;

public class RandomStringGenerator implements Generator<String> {
    private final int nbLetters;

    public RandomStringGenerator(final int nbLetters) {
        this.nbLetters = nbLetters;
    }

    ///// Fonction de l'interface Generator :

    @Override
    public String generate() {
        return random(this.nbLetters);
    }
}
