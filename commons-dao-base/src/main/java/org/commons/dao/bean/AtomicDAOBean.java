package org.commons.dao.bean;

import org.json.JSONObject;

import java.util.Objects;

public class AtomicDAOBean<K> extends AbstractDAOBean<K> {
    private K value;

    /// // Constructeur(s) :

    public AtomicDAOBean(final K value) {
        this.value = value;
    }

    /// // Méthodes

    @Override
    public int functionnalHashCode() {
        return Objects.hash(value);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof AtomicDAOBean<?> atomicDAOBean)) {
            return false;
        }
        return Objects.equals(value, atomicDAOBean.value);
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", value);
        return jsonObject;
    }

    public static <T> AbstractDAOBean<T> toAtomicDAOBean(final T value) {
        return new AtomicDAOBean<>(value);
    }

    /// // Getters & Setters :

    @Override
    public K getId() {
        return this.value;
    }

    @Override
    public void setId(final K id) {
        this.value = id;
    }
}
