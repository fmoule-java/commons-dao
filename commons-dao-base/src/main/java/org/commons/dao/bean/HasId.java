package org.commons.dao.bean;

public interface HasId<T> {

    /**
     * Méthode retournant l'id de l'instance. <br />
     *
     * @return id
     */
    T getId();

    /**
     * Méthode initialisant l'identifiant de l'instance. <br />
     *
     * @param id : nouvel identificnt
     */
    void setId(final T id);
}
