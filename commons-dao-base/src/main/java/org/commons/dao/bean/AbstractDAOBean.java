package org.commons.dao.bean;


import org.commons.dao.utils.json.ToJSON;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * <p>
 * Classe abstraite définissant les objets JAVA susceptibles d'être manipulé par
 * les DAOs. <br />
 * </p>
 *
 * @param <K>
 */
public abstract class AbstractDAOBean<K> implements ToJSON, HasId<K>, Cloneable {

    /**
     * Méthode permettant de retourner la clé. <br />
     *
     * @return clé de l'entité
     */
    public abstract K getId();

    /**
     * Permet d'initier la valeur
     *
     * @param id : clé de l'instance
     */
    public abstract void setId(final K id);

    /**
     * Calcul du code de hachage fonctionnelle. <br />
     *
     * @return code de hachage
     */
    public abstract int functionnalHashCode();

    /**
     * Méthode permettant de définir l'égalité fonctionnelle
     * entre deux instances.
     *
     * @param obj : objet à comparer
     * @return résultat de l'égalité fonctionnelle
     */
    public abstract boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj);


    /// // Méthode(s) de la classe

    @Override
    public int hashCode() {
        if (isNew()) {
            return functionnalHashCode();
        }
        return Objects.hashCode(this.getId());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(this.getClass().isInstance(obj))) {
            return false;
        }
        final AbstractDAOBean<?> that = (AbstractDAOBean<?>) obj;
        if (this.isNew() && that.isNew()) {
            return isFunctionnallyEquals(that);
        }
        return Objects.equals(this.getId(), that.getId());
    }

    @Override
    public AbstractDAOBean<K> clone() throws CloneNotSupportedException {
        try {
            //noinspection unchecked
            return (AbstractDAOBean<K>) super.clone();
        } catch (final CloneNotSupportedException cloneNotSupportedException) {
            throw new CloneNotSupportedException(cloneNotSupportedException.getMessage());
        }
    }

    public boolean isNew() {
        return (this.getId() == null);
    }

    /// // Clase(s) interne(s)

    /**
     * Prédicat permettant de tester si une instance de la classe AbstractDAOBean
     * est nouvelle ou pas c'est -à-dire devant être insérée ou à être mise à jour.
     *
     * @param <T> : type du bean
     */
    public static class IsNewPredicate<T extends AbstractDAOBean<?>> implements Predicate<T> {

        @Override
        public boolean test(final T abstractDAOBean) {
            if (abstractDAOBean == null) {
                return false;
            }
            return abstractDAOBean.isNew();
        }
    }
}
