package org.commons.dao.transcoder;

public interface Transcoder<U, V> {

    V transcode(final U bean) throws Exception;
}
