package org.commons.dao.collection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * DAO utilisant un Set pour enregistrer les "beans" en mémoire. <br />
 * Ce type de DAO peut être, donc, utiliser pour les tests unitaires.<br />
 * </p>
 *
 * @param <T> : type des objets gérés par le DAO.
 */
public class SetDAO<T> extends AbstractCollectionDAO<T> {
    protected final Set<T> beans;

    public SetDAO() {
        beans = new HashSet<>();
    }

    public SetDAO(final Collection<? extends T> beans) {
        this();
        this.addAll(beans);
    }

    ///// Getters & Setters :

    @Override
    protected Collection<T> getBeans() {
        return this.beans;
    }
}