package org.commons.dao.collection;

import org.commons.dao.base.AbstractDAO;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public abstract class AbstractCollectionDAO<T> extends AbstractDAO<T> {
    private final transient ReentrantReadWriteLock lock;

    public AbstractCollectionDAO() {
        super();
        lock = new ReentrantReadWriteLock();
    }

    ///// Méthodes abstraites :

    /**
     * Méthode retournant la collection sous-jacente des beans
     * gérés par le DAO.
     *
     * @return collection des beans
     */
    protected abstract Collection<T> getBeans();

    /// // Méthodes générales :

    public void addAll(final Collection<? extends T> beans) {
        this.lock.writeLock().lock();
        try {
            this.getBeans().addAll(beans);
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    public void clear() {
        this.lock.writeLock().lock();
        try {
            this.getBeans().clear();
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    /// // Méthodes de l'interface DAO :

    @Override
    public void close() {
        clear();
    }

    @Override
    public boolean isClosed() {
        this.lock.readLock().lock();
        try {
            return this.getBeans().isEmpty();
        } finally {
            this.lock.readLock().unlock();
        }
    }

    @Override
    public T save(final T bean) {
        this.lock.writeLock().lock();
        try {
            this.getBeans().add(bean);
            return bean;
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    @Override
    public List<T> select(final Predicate<T> predicate) {
        this.lock.readLock().lock();
        try {
            return this.getBeans()
                    .stream()
                    .filter(predicate)
                    .collect(toList());
        } finally {
            this.lock.readLock().unlock();
        }
    }

    @Override
    public void delete(final Predicate<T> predicate) {
        this.lock.writeLock().lock();
        try {
            final Set<T> beansToRemove = this.getBeans().stream().filter(predicate).collect(toSet());
            if (beansToRemove.isEmpty()) {
                return;
            }
            this.getBeans().removeAll(beansToRemove);
        } finally {
            this.lock.writeLock().unlock();
        }
    }

}
