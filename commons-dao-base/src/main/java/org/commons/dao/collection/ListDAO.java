package org.commons.dao.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ListDAO<T> extends AbstractCollectionDAO<T> {
    private final List<T> beans;

    public ListDAO() {
        this.beans = new ArrayList<>();
    }

    ///// Méthodes de l'interface DAO :

    @Override
    protected Collection<T> getBeans() {
        return this.beans;
    }
}
