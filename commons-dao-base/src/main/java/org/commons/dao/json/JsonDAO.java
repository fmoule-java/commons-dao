package org.commons.dao.json;

import org.commons.dao.base.AbstractDAO;
import org.commons.dao.base.DAO;
import org.commons.dao.predicate.NotNullPredicate;
import org.commons.dao.transcoder.Transcoder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * DAO permettant d'encapsuler un DAO existant afin de transformer
 * le résultat en objet JSON. <br />
 *
 * @param <T> : type de l'objet géré
 */
public class JsonDAO<T> extends AbstractDAO<JSONObject> {
    private final DAO<T> baseDAO;
    private final Transcoder<T, JSONObject> transcoder;

    public JsonDAO(final DAO<T> baseDAO, final Transcoder<T, JSONObject> transcoder) {
        requireNonNull(baseDAO, "Le DAO de base ne doit pas être nul");
        requireNonNull(transcoder, "Le transcoder de base ne doit pas être nul");
        this.baseDAO = baseDAO;
        this.transcoder = transcoder;
    }

    ///// Méthodes de l'interface DAO :

    @Override
    public JSONObject save(final JSONObject bean) {
        throw new RuntimeException("A Implémenter !!");
    }

    @Override
    public List<JSONObject> select(final Predicate<JSONObject> predicate) throws Exception {
        final List<JSONObject> jsonObjects = new ArrayList<>();
        JSONObject jsonBean;
        for (T bean : this.baseDAO.selectAll()) {
            jsonBean = transcoder.transcode(bean);
            if (predicate.test(jsonBean)) {
                jsonObjects.add(jsonBean);
            }
        }
        return jsonObjects;
    }

    @Override
    public void delete(final Predicate<JSONObject> predicate) throws Exception {
        final Set<T> beansToRemove = new HashSet<>();
        for (T bean : this.baseDAO.selectAll()) {
            if (predicate.test(this.transcoder.transcode(bean))) {
                beansToRemove.add(bean);
            }
        }
        this.baseDAO.delete(beansToRemove);
    }

    ///// Méthodes générales :

    public JSONArray selectAsJSONArray(final Predicate<JSONObject> predicate) throws Exception {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonBean;
        for (T bean : this.baseDAO.selectAll()) {
            jsonBean = transcoder.transcode(bean);
            if (predicate.test(jsonBean)) {
                jsonArray.put(jsonBean);
            }
        }
        return jsonArray;
    }

    public JSONArray selectAsJSONArray(final Predicate<JSONObject> predicate, final Comparator<T> baseBeanComparator) throws Exception {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonBean;
        for (T bean : this.baseDAO.selectAll(baseBeanComparator)) {
            jsonBean = transcoder.transcode(bean);
            if (predicate.test(jsonBean)) {
                jsonArray.put(jsonBean);
            }
        }
        return jsonArray;
    }

    @Override
    public void reload() throws Exception {
        this.baseDAO.reload();
    }

    @Override
    public boolean isClosed() {
        return (this.baseDAO == null || this.baseDAO.isClosed());
    }

    @Override
    public void close() throws IOException {
        if (!this.isClosed()) {
            this.baseDAO.close();
        }
    }

    ///// Méthodes générales :

    public JSONArray selectAllAsJSONArray() throws Exception {
        return this.selectAsJSONArray(new NotNullPredicate<>());
    }

    public JSONArray selectAllAsJSONArray(final Comparator<T> baseBeanComparator) throws Exception {
        return this.selectAsJSONArray(new NotNullPredicate<>(), baseBeanComparator);
    }

    ///// Getters & Setters :

    /**
     * Getter permettant de retourner le DAO de base. <br />
     *
     * @return DAO de base
     */
    public DAO<T> getBaseDAO() {
        return baseDAO;
    }
}
