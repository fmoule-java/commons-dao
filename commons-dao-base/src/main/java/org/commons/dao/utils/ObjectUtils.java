package org.commons.dao.utils;

public class ObjectUtils {

    private ObjectUtils() {
        // EMPTY
    }

    /// // Méthode(s) statique(s)

    public static <T> T cast(final Object obj, final Exception exception) throws Exception {
        try {
            //noinspection unchecked
            return (T) obj;
        } catch (final ClassCastException classCastException) {
            throw exception;
        }
    }
}
