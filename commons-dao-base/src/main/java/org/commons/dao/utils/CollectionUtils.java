package org.commons.dao.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

public class CollectionUtils {

    private CollectionUtils() {
        // EMPTY
    }

    /// // Méthode(s) statique(s)

    public static <T> List<T> filterCollection(final Collection<? extends T> beans,
                                               final Predicate<? super T> predicate) {
        if (isEmpty(beans) || (predicate == null)) {
            return new ArrayList<>();
        }
        return beans.stream().filter(predicate).collect(toList());
    }
}
