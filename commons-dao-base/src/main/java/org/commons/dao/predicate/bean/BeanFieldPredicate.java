package org.commons.dao.predicate.bean;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.function.Predicate;

import static org.commons.dao.utils.reflection.ReflectionUtils.getGetter;

/**
 * Prédicat permettant de tester, par reflexisivité, la valeur d'un champs. <br />
 *
 * @param <T> : type des objets manipulés par le prédicat.
 * @author Frédéric Moulé
 * @see Predicate
 */
public class BeanFieldPredicate<T> implements Predicate<T> {
    private final String fieldName;
    private final Object fieldValue;

    /// // Constructeur(s)

    /**
     * @param fieldName  : nom du champs
     * @param fieldValue : valeur du champs
     */
    public BeanFieldPredicate(final String fieldName, final Object fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    /// // Méthode(s) de l'interface Predicat

    @Override
    public boolean test(final T bean) {
        try {
            if (bean == null) {
                return false;
            }
            final StringTokenizer tokenizer = new StringTokenizer(fieldName, ".", false);
            String theFieldName = "";
            Method getter;
            Object theBean = bean;
            while (tokenizer.hasMoreTokens()) {
                theFieldName = tokenizer.nextToken();
                getter = getGetter(theBean.getClass(), theFieldName);
                if (getter == null) {
                    return false;
                }
                theBean = getter.invoke(theBean);
            }
            return Objects.equals(this.fieldValue, theBean);
        } catch (final Exception exception) {
            return false;
        }
    }

    /// // Méthode(s) statique(s) :

    /**
     * Méthode retournant le prédicat testant si la valeur du champs est égale
     * à une valeur donnée
     *
     * @param fieldName  : nom du champs
     * @param fieldValue : valeur du champs
     * @param <U>        : type des objets manipulés par le prédicat
     * @return prédicat
     */
    public static <U> BeanFieldPredicate<U> withBeanFieldEqualsTo(final String fieldName, final Object fieldValue) {
        return new BeanFieldPredicate<>(fieldName, fieldValue);
    }

    /// // Getters :

    /**
     * Retourne le nom du champs. <br />
     *
     * @return nom du champs
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Retourne la valeur du champs. <br />
     *
     * @return valeur du champs
     */
    public Object getFieldValue() {
        return fieldValue;
    }
}
