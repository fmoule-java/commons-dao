package org.commons.dao.predicate;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * <p>
 * Prédicat permettant de tester l'égalité avec un objet passé au constructeur. <br />
 * </p>
 *
 * @param <T> : type des objets manipulés
 */
public class EqualsPredicate<T> implements Predicate<T> {
    private T initBean;

    /// // Constructeurs :

    public EqualsPredicate(final T initBean) {
        this.initBean = initBean;
    }

    public EqualsPredicate() {
        this(null);
    }

    /// // Méthode(s) de l'interface :

    @Override
    public boolean test(final T bean) {
        return Objects.equals(this.initBean, bean);
    }

    /// // Getters & Setters :

    public void setInitBean(final T initBean) {
        this.initBean = initBean;
    }
}
