package org.commons.dao.predicate;

import java.util.function.Predicate;

/**
 * Prédicat permettant de tester si l'objet passé en argument est non null.<br />
 *
 * @param <T> : type des objets
 */
public class NotNullPredicate<T> implements Predicate<T> {

    @Override
    public boolean test(final T bean) {
        return bean != null;
    }
}
