package org.commons.dao.predicate.operator;

import java.util.List;
import java.util.function.Predicate;

import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.AND;
import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.OR;

/**
 * @param <T> : type des objets manipulés
 */
public record OperatorPredicate<T, P extends Predicate<? super T>>(OperatorPredicate.PredicateOperator getOperator,
                                                                   List<P> predicates) implements Predicate<T> {

    /// // Méthode de l'interface Predicate :

    @Override
    public boolean test(final T bean) {
        Boolean isOK = null;
        for (P predicate : predicates) {
            if (isOK == null) {
                isOK = predicate.test(bean);
            } else if (getOperator == AND) {
                isOK = isOK && predicate.test(bean);
            } else if (getOperator == OR) {
                isOK = isOK || predicate.test(bean);
            }
        }
        return (isOK != null && isOK);
    }

    @Override
    public Predicate<T> and(final Predicate<? super T> predicate) {
        if (predicate == null) {
            return this;
        }
        return new OperatorPredicate<>(AND, List.of(this, predicate));
    }

    @Override
    public Predicate<T> or(final Predicate<? super T> predicate) {
        if (predicate == null) {
            return this;
        }
        return new OperatorPredicate<>(OR, List.of(this, predicate));
    }

    @Override
    public Predicate<T> negate() {
        return new OperatorPredicate<>(this.getOperator.negate(),
                this.predicates().stream().map(Predicate::negate).toList());
    }

    /// // Méthodes statiques :

    @SafeVarargs
    public static <T> Predicate<T> and(final Predicate<T>... predicates) {
        Predicate<T> andPredicate = null;
        if (predicates == null) {
            return andPredicate;
        }
        for (Predicate<T> predicate : predicates) {
            if (andPredicate == null) {
                andPredicate = predicate;
                continue;
            }
            andPredicate = andPredicate.and(predicate);
        }
        return andPredicate;
    }

    @SafeVarargs
    public static <T> Predicate<T> or(final Predicate<T>... predicates) {
        Predicate<T> andPredicate = null;
        if (predicates == null) {
            return andPredicate;
        }
        for (Predicate<T> predicate : predicates) {
            if (andPredicate == null) {
                andPredicate = predicate;
                continue;
            }
            andPredicate = andPredicate.or(predicate);
        }
        return andPredicate;
    }

    /// // Getters

    public PredicateOperator getOperator() {
        return getOperator;
    }

    /// // Classe(s) interne(s) :

    public enum PredicateOperator {
        AND,
        OR;

        public PredicateOperator negate() {
            if (this == AND) {
                return OR;
            } else {
                return AND;
            }
        }
    }
}
