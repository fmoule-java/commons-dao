package org.commons.dao.predicate;

import org.commons.dao.bean.HasId;

import java.util.Objects;
import java.util.function.Predicate;

/**
 *
 * @param <S> : type de l'identifiant
 * @param <T> : type de l'objet manipulé par le prédicat
 * @see Predicate
 * @see HasId
 */
public class IdPredicate<S, T extends HasId<S>> implements Predicate<T> {
    private final S id;

    /// // Constructeur(s) :

    public IdPredicate(final S id) {
        this.id = id;
    }

    /// // Méthode(s) de l'interface Predicat

    @Override
    public boolean test(final T bean) {
        if (bean == null) {
            return false;
        }
        return Objects.equals(id, bean.getId());
    }

    /**
     * @param idEvent : id à comparer
     * @param <S>     : type de l'id
     * @param <T>     : type de l'objet
     * @return prédicat
     */
    public static <S, T extends HasId<S>> IdPredicate<S, T> withIdEqualTo(final S idEvent) {
        return new IdPredicate<>(idEvent);
    }
}
