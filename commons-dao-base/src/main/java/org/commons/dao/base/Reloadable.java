package org.commons.dao.base;

/**
 * Cette interface permet de déterminer tous les objets "rechargeables". <br />
 */
public interface Reloadable {

    /**
     * Recharge.
     *
     * @throws Exception : en cas de problème
     */
    void reload() throws Exception;
}
