package org.commons.dao.base;

public interface Startable {

    /**
     * Méthode permettant de démarrer l'instance courante. <br />
     *
     * @throws Exception : En cas de problème lors du démarrage
     */
    void start() throws Exception;
}
