package org.commons.dao.base;

import org.commons.dao.bean.HasId;
import org.commons.dao.predicate.EqualsPredicate;
import org.commons.dao.predicate.IdPredicate;
import org.commons.dao.predicate.NotNullPredicate;
import org.slf4j.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNullElseGet;
import static java.util.Optional.empty;

/**
 * Data Access Object pour les objets du pricer. <br />
 *
 * @param <T> : type d'objet manipulé
 */
public interface DAO<T> extends Closeable, Startable, Reloadable {

    /**
     * Méthode permettant de retourner la liste des éléments correspondant
     * au prédicat passé en paramètre. <br />
     *
     * @param predicate : prédicat
     * @return liste des éléments
     * @throws Exception : En cas de problème
     * @see Predicate
     */
    List<T> select(final Predicate<T> predicate) throws Exception;

    /**
     * <p>
     * Sauvegarde l'objet passé en paramètre de la méthode. <br />
     * La méthode retourne l'objet sauvegardé agrémenté potentiellement de champs initialisés par la couche de persistance.
     * </p>
     *
     * @param bean : objet à sauvegarder
     * @return objet sauvegardé
     * @throws Exception : En cas de problème lors de la sauvegarde
     */
    T save(final T bean) throws Exception;

    /**
     * Méthode permettant de supprimer un élément. <br />
     *
     * @param predicate : prédicat permettant de déterminer les éléments à supprimer.
     * @throws Exception : En cas de problème
     */
    void delete(final Predicate<T> predicate) throws Exception;

    /**
     * Méthode permettant de montrer si l'instance courant eestr
     * fermée. <br />
     *
     * @return booléen montrant le résultat
     */
    boolean isClosed();

    /**
     * Méthode permettant de retourner le logger
     * utilisé par l'instance courant du DAO. <br />
     *
     * @return instance du logger
     */
    Logger getLogger();

    ///// Implémentations par défaut :

    /**
     * Méthode permettant de montrer si l'instance est "ouverte". <br />
     * Cela correspond à l'opposé de la méthode <b>isClosed</b><br />
     *
     * @return booléen montrant si le DAO est ouvert ou actif
     */
    default boolean isOpen() {
        return !this.isClosed();
    }


    default Collection<T> save(final Collection<? extends T> beans) throws Exception {
        final HashSet<T> savedEntities = new HashSet<>();
        if (beans == null || beans.isEmpty()) {
            return savedEntities;
        }
        for (T bean : beans) {
            savedEntities.add(this.save(bean));
        }
        return savedEntities;
    }

    /**
     * <p>
     * Méthode permettant de sélectionner puis de trier les éléments voulus.
     * </p>
     *
     * @param predicate  : prédicat permettant de sélectionner les éléments
     * @param comparator : comparateur permettant le tri des élements
     * @return liste des éléments
     * @throws Exception : En cas de problème
     */
    default Collection<T> select(final Predicate<T> predicate, final Comparator<T> comparator) throws Exception {
        final TreeSet<T> beans = new TreeSet<>(requireNonNullElseGet(comparator, ToStringComparator::new));
        beans.addAll(this.select(predicate));
        return beans;
    }

    /**
     * Méthode permettant de sélectionner le premier élément trouvé correspondant au prédicat. <br />
     *
     * @param predicate : prédicat
     * @return élément trouvé
     * @throws Exception : en cas d'exception
     */
    default Optional<T> selectFirst(final Predicate<T> predicate) throws Exception {
        final List<T> foundElements = this.select(predicate);
        if (foundElements == null || foundElements.isEmpty()) {
            return empty();
        }
        return Optional.of(foundElements.get(0));
    }

    /**
     * Méthode permettant de récupérer le premuier élément
     * en fonction du prédicate et du comparateur passés en paramètres. <br />
     *
     * @param predicate  : prédicat
     * @param comparator : comparateur
     * @return premier élément sous forme d'optional
     * @throws Exception : En cas de problème lors du traitement
     * @see Optional
     */
    default Optional<T> selectFirst(final Predicate<T> predicate, final Comparator<T> comparator) throws Exception {
        final Collection<T> foundBeans = this.select(predicate, comparator);
        for (T foundBean : foundBeans) {
            return Optional.of(foundBean);
        }
        return Optional.empty();
    }

    /**
     * Méthode permettant de compter le nombre d'éléments
     * correspondant au prédicat passé en paramètre de la fonction. <br />
     *
     * @param predicate : prédicat
     * @return nombre d'éléments
     * @throws Exception : En cas de problème
     */
    default long selectCount(final Predicate<T> predicate) throws Exception {
        return ((Set<T>) new HashSet<>(this.select(predicate))).size();
    }

    /**
     * Fonction permettant de retourner le nombre d'éléments
     * présents dans la couche de persistance. <br />
     *
     * @return nombre d'éléments
     * @throws Exception : En cas de problème
     */
    default long selectCount() throws Exception {
        final List<T> allBeans = this.selectAll();
        return (allBeans == null ? 0L : allBeans.size());
    }

    /**
     * Permet de sélectionner les objets suivant un tri pré-déterminé. <br />
     *
     * @param comparator : comparateur
     * @return collection de tous les beans, triée suivant le comparateur.
     * @throws Exception : En cas de problème
     */
    default Collection<T> selectAll(final Comparator<T> comparator) throws Exception {
        final TreeSet<T> beans = new TreeSet<>(comparator);
        beans.addAll(this.selectAll());
        return beans;
    }

    default List<T> selectAll() throws Exception {
        return this.select(Objects::nonNull);
    }

    default void delete(final Collection<? extends T> beans) throws Exception {
        if (beans == null || beans.isEmpty()) {
            return;
        }
        final EqualsPredicate<T> eqPredicate = new EqualsPredicate<>();
        for (T bean : beans) {
            eqPredicate.setInitBean(bean);
            this.delete(eqPredicate);
        }
    }

    /**
     * Méthode permettant de supprimer tous les objets du DAO. <br />
     *
     * @throws Exception : en cas d'exception
     */
    default void deleteAll() throws Exception {
        this.delete(new NotNullPredicate<>());
    }

    @Override
    default void close() throws IOException {
        // DO NOTHING !!
    }

    default void start() throws Exception {
        // DO NOTHING !!
    }

    /**
     * Redémarrage du DAO. <br />
     *
     * @throws Exception : En cas de problème lors du redémarrage.
     */
    default void reload() throws Exception {
        this.close();
        this.start();
    }

    /// // Méthodes utilitaires :

    static <ID, B extends HasId<ID>> Optional<B> fetchByID(final DAO<B> dao, final ID id) throws Exception {
        if (id == null) {
            return empty();
        }
        return fetchFirstElementFromDAO(dao, new IdPredicate<>(id));
    }

    /**
     * Sélectionne le premier élément du DAO suivant le prédicat passé en
     * paramètre. <br />
     *
     * @param dao       : DAO utilisé pour récupérer les données
     * @param predicate : prédicat pour la sélection
     * @param <B>       : type d'objet
     * @return premier objet du DAO
     * @throws Exception : En cas d'exception
     */
    static <B> Optional<B> fetchFirstElementFromDAO(final DAO<B> dao,
                                                                               final Predicate<B> predicate) throws Exception {
        if (dao == null) {
            return empty();
        }
        return dao.selectFirst(predicate);
    }

    /**
     * Ferme le DAO. <br />
     *
     * @param theDAO : DAO
     * @param <B>    : type du DAO
     * @throws IOException : En cas d'exception I/O
     */
    static <B> void closeDAO(final DAO<B> theDAO) throws IOException {
        if (theDAO != null && !theDAO.isClosed()) {
            theDAO.close();
        }
    }

    /**
     * Ferme le DAO. <br />
     *
     * @param theDAO : DAO
     * @param <B>    : type du DAO
     * @throws IOException : En cas d'exception I/O
     */
    static <B> void clearAndCloseDAO(final DAO<B> theDAO) throws Exception {
        if (theDAO == null || theDAO.isClosed()) {
            return;
        }
        theDAO.deleteAll();
        theDAO.close();
    }


}
