package org.commons.dao.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Classe de base permettant d'implémenter les DAOs. <br />
 * </p>
 *
 * @param <T> : type des entités gérées par le DAO
 */
public abstract class AbstractDAO<T> implements DAO<T> {
    protected Logger logger;

    /// // Constructeur(s)

    public AbstractDAO() {
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    /// // Getters & Setters

    @Override
    public Logger getLogger() {
        return this.logger;
    }

    /**
     * Méthode permettant de spécifier
     * le logger utilisé. <br />
     *
     * @param logger : logger
     */
    public void setLogger(final Logger logger) {
        this.logger = logger;
    }
}
