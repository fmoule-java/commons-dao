package org.commons.dao.base;

import java.util.Comparator;

public class ToStringComparator<T> implements Comparator<T> {
    private static final String EMPTY_STRING = "";

    private static String toString(final Object bean) {
        return (bean == null ? EMPTY_STRING : bean.toString());
    }

    @Override
    public int compare(final T bean1, final T bean2) {
        return toString(bean1).compareTo(toString(bean2));
    }
}
