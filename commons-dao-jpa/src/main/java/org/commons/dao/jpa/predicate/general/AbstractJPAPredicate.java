package org.commons.dao.jpa.predicate.general;

import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jpa.predicate.JPAPredicate;
import org.commons.dao.predicate.operator.OperatorPredicate;
import org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator;

import java.util.List;
import java.util.function.Predicate;

import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.AND;
import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.OR;

/**
 * Classe de base des différentes instances de l'interface JPAPredicate
 *
 * @param <T>  : Type des données manipulées par le prédicat
 * @param <P>: Type du prédicat de base utilisé
 */
public abstract class AbstractJPAPredicate<T extends AbstractDAOBean<?>, P extends Predicate<? super T>>
        implements JPAPredicate<T> {
    protected final P basePredicate;

    /// // Constructeur(s)

    public AbstractJPAPredicate(final P predicate) {
        this.basePredicate = predicate;
    }

    /// // Méthode(s) privée(s)

    protected Predicate<T> combinePredicateFromOperator(final PredicateOperator operator, final Predicate<? super T> otherPredicate) {
        if (otherPredicate instanceof JPAPredicate<? super T> jpaPredicate) {
            return new JPAOperatorPredicate<>(operator, List.of(this, jpaPredicate));
        }
        return new OperatorPredicate<>(operator, List.of(this, otherPredicate));
    }

    /// // Définition des opérateurs

    @Override
    public Predicate<T> and(final Predicate<? super T> predicate) {
        return combinePredicateFromOperator(AND, predicate);
    }

    @Override
    public Predicate<T> or(Predicate<? super T> other) {
        return combinePredicateFromOperator(OR, other);
    }

    /// // Méthode(s) de l'interface JPAPredicate :

    @Override
    public boolean test(final T bean) {
        return basePredicate.test(bean);
    }

}
