package org.commons.dao.jpa;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.SharedCacheMode;
import jakarta.persistence.ValidationMode;
import jakarta.persistence.spi.ClassTransformer;
import jakarta.persistence.spi.PersistenceUnitInfo;
import jakarta.persistence.spi.PersistenceUnitTransactionType;
import org.hibernate.jpa.HibernatePersistenceProvider;

import javax.sql.DataSource;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static jakarta.persistence.spi.PersistenceUnitTransactionType.RESOURCE_LOCAL;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * Classe regroupant les fonctions utilitaires concernant la couche
 * de persistence JPA. <br />
 * </p>
 */
public class JPAUtils {
    private static final HibernatePersistenceProvider HIBERNATE_PERSISTENCE_PROVIDER = new HibernatePersistenceProvider();

    private JPAUtils() {
        // EMPTY
    }

    /// // Fonctions utiles

    /**
     * <p>
     * Méthode permettant de créer une instance de la classe EntityManagerFactory
     * </p>
     *
     * @param unitName      : nom de l'unité
     * @param entityClasses : classes des entités gérés par les 'entityManager'
     * @return instance de la classe
     * @see EntityManagerFactory
     */
    public static EntityManagerFactory createEntityManagerFactory(final String unitName,
                                                                  final DataSource dataSource,
                                                                  final Set<Class<?>> entityClasses) {
        return HIBERNATE_PERSISTENCE_PROVIDER
                .createContainerEntityManagerFactory(new DefaultPersistenceUnitInfo(unitName, dataSource, entityClasses), new HashMap<>());
    }

    /// // Classe(s) interne(s) :

    private static class DefaultPersistenceUnitInfo implements PersistenceUnitInfo {
        private final String unitName;
        private final DataSource dataSource;
        private final Set<Class<?>> entityClasses;

        public DefaultPersistenceUnitInfo(final String unitName,
                                          final DataSource dataSource,
                                          final Set<Class<?>> entityClasses) {
            this.unitName = unitName;
            this.dataSource = dataSource;
            this.entityClasses = entityClasses;
        }

        @Override
        public String getPersistenceUnitName() {
            return unitName;
        }

        @Override
        public String getPersistenceProviderClassName() {
            return HibernatePersistenceProvider.class.getName();
        }

        @Override
        public PersistenceUnitTransactionType getTransactionType() {
            return RESOURCE_LOCAL;
        }


        @Override
        public DataSource getJtaDataSource() {
            return this.dataSource;
        }

        @Override
        public DataSource getNonJtaDataSource() {
            return null;
        }

        @Override
        public List<String> getMappingFileNames() {
            return List.of();
        }

        @Override
        public List<URL> getJarFileUrls() {
            return List.of();
        }

        @Override
        public URL getPersistenceUnitRootUrl() {
            return null;
        }

        @Override
        public List<String> getManagedClassNames() {
            if (entityClasses == null) {
                return List.of();
            }
            return entityClasses.stream().map(Class::getName).collect(toList());
        }

        @Override
        public boolean excludeUnlistedClasses() {
            return false;
        }

        @Override
        public SharedCacheMode getSharedCacheMode() {
            return null;
        }

        @Override
        public ValidationMode getValidationMode() {
            return null;
        }

        @Override
        public Properties getProperties() {
            return new Properties();
        }

        @Override
        public String getPersistenceXMLSchemaVersion() {
            return "";
        }

        @Override
        public ClassLoader getClassLoader() {
            return null;
        }

        @Override
        public void addTransformer(ClassTransformer transformer) {

        }

        @Override
        public ClassLoader getNewTempClassLoader() {
            return null;
        }
    }
}
