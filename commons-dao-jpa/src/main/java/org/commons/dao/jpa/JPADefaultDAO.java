package org.commons.dao.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.commons.dao.base.AbstractDAO;
import org.commons.dao.base.DAO;
import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jpa.predicate.JPAPredicate;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

/**
 * <p>
 * DAO basé sur les instances <b>EntityManager</b>
 * </p>
 *
 * @param <T> : type des entité
 * @author Frédéric Moulé
 * @see DAO
 * @see EntityManager
 */
public class JPADefaultDAO<T extends AbstractDAOBean<?>> extends AbstractDAO<T> {
    private final EntityManager entityManager;
    private final Class<T> entityClass;

    /// Constructeur(s) :

    /**
     * Constructeur à partir d'une instance <p>EntityManager</p>
     *
     * @param entityManager : entity manager
     * @see EntityManager
     */
    public JPADefaultDAO(final EntityManager entityManager, final Class<T> entityClass) {
        this.entityManager = entityManager;
        this.entityClass = entityClass;
    }

    public JPADefaultDAO(final EntityManagerFactory emf, final Class<T> entityClass) {
        this((emf == null ? null : emf.createEntityManager()), entityClass);
    }

    /// Méthodes de l'interface Closeable

    /**
     * Ferme la connexion
     */
    @Override
    public void close() {
        if (entityManager != null) {
            this.entityManager.close();
        }
    }

    @Override
    public boolean isClosed() {
        return this.entityManager != null
                && !this.entityManager.isOpen();
    }

    /// // Définition des méthodes de la clase DAO :

    @Override
    public List<T> select(final Predicate<T> predicate) throws Exception {
        if (predicate instanceof JPAPredicate<T> jpaPredicate) {
            return this.entityManager
                    .createQuery(buildCriteriaQuery(jpaPredicate))
                    .getResultList();
        }
        return this.selectAll()
                .stream()
                .filter(predicate)
                .collect(toList());
    }

    protected CriteriaQuery<T> buildCriteriaQuery(final JPAPredicate<T> jpaPredicate) throws Exception {
        if (entityManager == null) {
            return null;
        }
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(entityClass);
        final Root<T> root = query.from(entityClass);
        query = query.where(jpaPredicate.buildWherePredicate(criteriaBuilder, root));
        return query;
    }

    @Override
    public List<T> selectAll() throws Exception {
        if (entityManager == null) {
            throw new Exception("L'entityManager est nul !!");
        } else if (!entityManager.isOpen()) {
            throw new Exception("L'entityManager est fermé !!");
        }
        this.entityManager.clear();
        return this.entityManager
                .createQuery("SELECT b FROM " + entityClass.getSimpleName() + " b", entityClass)
                .getResultList();
    }

    @Override
    public T save(final T bean) throws Exception {
        if (bean == null) {
            return null;
        }
        final EntityTransaction transaction = this.entityManager.getTransaction();
        transaction.begin();
        try {
            final T savedBean = this.entityManager.merge(bean);
            this.entityManager.flush();
            transaction.commit();
            //noinspection unchecked
            return (T) savedBean.clone();
        } catch (final ClassCastException classCastException) {
            this.getLogger().error("Problème avec le clonage : {}", classCastException.getMessage());
            throw classCastException;
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        }
    }

    @Override
    public Collection<T> save(final Collection<? extends T> beans) throws Exception {
        if (isEmpty(beans)) {
            return new HashSet<>();
        }
        final EntityTransaction transaction = this.entityManager.getTransaction();
        transaction.begin();
        try {
            final Set<T> savedBeans = new HashSet<>();
            T savedBean;
            for (T bean : beans) {
                if (bean == null) {
                    continue;
                }
                savedBean = this.entityManager.merge(bean);
                //noinspection ReassignedVariable,unchecked
                savedBeans.add((T) savedBean.clone());
            }
            this.entityManager.flush();
            transaction.commit();
            return savedBeans;
        } catch (final ClassCastException classCastException) {
            transaction.rollback();
            this.getLogger().error("Probleme probable avec le cast du clonage : {}", classCastException.getMessage());
            throw classCastException;
        } catch (final Exception exception) {
            transaction.rollback();
            this.getLogger().error("ERREUR : {}", exception.getMessage());
            throw exception;
        }
    }

    @Override
    public void delete(final Predicate<T> predicate) throws Exception {
        final List<T> beansToDelete = this.select(predicate);
        if (beansToDelete.isEmpty()) {
            return;
        }
        this.entityManager.getTransaction().begin();
        try {
            for (T bean : beansToDelete) {
                this.entityManager.remove(bean);
            }
            this.entityManager.getTransaction().commit();
        } catch (final Exception exception) {
            this.entityManager.getTransaction().rollback();
            throw exception;
        }

    }

    ///// Getters & Setters :

    /**
     * Permet de retourner la classe de base des objets
     * manipulés par le DAO. <br />
     *
     * @return classe
     */
    public Class<T> getEntityClass() {
        return entityClass;
    }
}
