package org.commons.dao.jpa.predicate.general;

import jakarta.persistence.criteria.*;
import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.predicate.bean.BeanFieldPredicate;

import java.util.StringTokenizer;

import static jakarta.persistence.criteria.JoinType.LEFT;

/**
 * <p>
 * Predicate JPA permettant de tester l'égalité de la valeur d'un champ d'un bean
 * avec une valeur donnée. <br >/
 * </p>
 *
 * @param <T>: type des objets manipulés
 * @see BeanFieldPredicate
 */
public class JPAFieldPredicate<T extends AbstractDAOBean<?>>
        extends AbstractJPAPredicate<T, BeanFieldPredicate<T>> {

    /// // Constructeur(s)

    /**
     * @param fieldName  : nom du champs
     * @param fieldValue : valeur du champs
     */
    public JPAFieldPredicate(final String fieldName,
                             final Object fieldValue) {
        super(new BeanFieldPredicate<>(fieldName, fieldValue));
    }

    /// // Méthode(s) générale(s)

    @Override
    public Predicate buildWherePredicate(final CriteriaBuilder criteriaBuilder, final Root<?> root) {
        final String fieldName = basePredicate.getFieldName();
        final StringTokenizer fieldTokenizer = new StringTokenizer(fieldName, ".", false);
        Path<?> path = root;
        Fetch<?, ?> fetchPart = null;
        String theFieldName;
        while (fieldTokenizer.countTokens() > 1) {
            theFieldName = fieldTokenizer.nextToken();
            if (fetchPart == null) {
                fetchPart = root.fetch(theFieldName, LEFT);
            } else {
                fetchPart = fetchPart.fetch(theFieldName, LEFT);
            }
            path = path.get(theFieldName);
        }
        path = path.get(fieldTokenizer.nextToken());
        return criteriaBuilder.equal(path, basePredicate.getFieldValue());
    }

    /// // Méthode(s) statique(s)

    public static <T extends AbstractDAOBean<?>> JPAFieldPredicate<T> withJPAFieldEqualsTo(final String fieldName,
                                                                                           final Object fieldValue) {
        return new JPAFieldPredicate<>(fieldName, fieldValue);
    }

}
