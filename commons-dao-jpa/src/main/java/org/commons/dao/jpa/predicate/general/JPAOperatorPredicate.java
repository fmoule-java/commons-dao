package org.commons.dao.jpa.predicate.general;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Root;
import org.commons.dao.bean.AbstractDAOBean;
import org.commons.dao.jpa.predicate.JPAPredicate;
import org.commons.dao.predicate.operator.OperatorPredicate;
import org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator;

import java.util.ArrayList;
import java.util.List;

import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.AND;
import static org.commons.dao.predicate.operator.OperatorPredicate.PredicateOperator.OR;

public class JPAOperatorPredicate<T extends AbstractDAOBean<?>>
        extends AbstractJPAPredicate<T, OperatorPredicate<T, JPAPredicate<? super T>>> {

    /// // Constructeur(s)

    public JPAOperatorPredicate(final PredicateOperator predicateOperator, final List<JPAPredicate<? super T>> predicates) {
        //noinspection Convert2Diamond
        super(new OperatorPredicate<T, JPAPredicate<? super T>>(predicateOperator, predicates));
    }

    /// // Méthode de la classe AbstractJPAPredicate

    @Override
    public jakarta.persistence.criteria.Predicate buildWherePredicate(final CriteriaBuilder criteriaBuilder, final Root<?> root) throws Exception {
        final List<jakarta.persistence.criteria.Predicate> criteriaPredicates = new ArrayList<>();
        for (JPAPredicate<? super T> jpaPredicate : this.basePredicate.predicates()) {
            criteriaPredicates.add(jpaPredicate.buildWherePredicate(criteriaBuilder, root));
        }
        jakarta.persistence.criteria.Predicate resultPredicate = null;
        if (this.basePredicate.getOperator() == AND) {
            resultPredicate = criteriaBuilder.and(criteriaPredicates.toArray(jakarta.persistence.criteria.Predicate[]::new));
        } else if (this.basePredicate.getOperator() == OR) {
            resultPredicate = criteriaBuilder.or(criteriaPredicates.toArray(jakarta.persistence.criteria.Predicate[]::new));
        }
        return resultPredicate;
    }
}
