package org.commons.dao.jpa.predicate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Root;
import org.commons.dao.bean.AbstractDAOBean;

import java.util.function.Predicate;

/**
 * <p>
 * Prdéicat JPA permettant une utilisation optimale des DAO JPA.<br />
 * </p>
 *
 * @param <T> : type des objets manipulés par le prédicat.
 * @author Frédéric Moulé
 */
public interface JPAPredicate<T extends AbstractDAOBean<?>> extends Predicate<T> {


    /**
     * Permet de récupérer la condition sous la forme d'un prédicat
     * de l'API Persistence. <br />
     * @param criteriaBuilder
     * @param root
     * @return instance du prédicat
     * @see jakarta.persistence.criteria.Predicate
     */
    jakarta.persistence.criteria.Predicate buildWherePredicate(final CriteriaBuilder criteriaBuilder,
                                                               final Root<?> root) throws Exception;
}
