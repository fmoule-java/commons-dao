package org.commons.dao.jpa.beans;

import jakarta.persistence.*;
import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static jakarta.persistence.CascadeType.*;
import static jakarta.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "company")
public class CompanyEntity extends AbstractDAOBean<Long> {

    @Id
    @GeneratedValue (strategy = IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = "country_id")
    private CountryEntity country;

    @OneToMany(mappedBy = "company")
    private Set<PersonEntity> employees;

    /// // Constructeur(s) :

    public CompanyEntity(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public CompanyEntity(final String name) {
        this(null, name);
    }

    public CompanyEntity() {
        this(null);
    }

    /// // Méthode(s) de la classe AsbtractDAOBean :

    @Override
    public int functionnalHashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof CompanyEntity that)) {
            return false;
        }
        return Objects.equals(name, that.name);
    }

    @Override
    public CompanyEntity clone() throws CloneNotSupportedException {
        return (CompanyEntity) super.clone();
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonCompany = new JSONObject();
        jsonCompany.put("id", id);
        jsonCompany.put("name", name);
        if (country != null) {
            jsonCompany.put("country", country.toJSON());
        }
        return jsonCompany;
    }

    /// // Getters & Setters :

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public void setEmployees(final Collection<? extends PersonEntity> employees) {
        this.employees = new HashSet<>(employees);
    }

    public Set<PersonEntity> getEmployees() {
        return (employees == null ? new HashSet<>() : employees);
    }

    public void setName(final String name) {
        this.name = (name == null ? null : name.trim());
    }

    public String getName() {
        return this.name;
    }

    public void setCountry(final CountryEntity countryEntity) {
        this.country = countryEntity;
    }

    public CountryEntity getCountry() {
        return this.country;
    }
}
