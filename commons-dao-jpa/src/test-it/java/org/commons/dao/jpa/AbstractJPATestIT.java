package org.commons.dao.jpa;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.EntityManagerFactory;

import java.io.FileReader;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.commons.dao.jpa.JPAUtils.createEntityManagerFactory;
import static org.commons.dao.utils.reflection.ReflectionUtils.listPackageClasses;

public abstract class AbstractJPATestIT {
    private static final String DEFAULT_PROPERTIES_FILE_PATH = AbstractJPATestIT.class.getResource("/application.properties").getPath();
    private final Properties configProperties = new Properties();
    protected EntityManagerFactory entityManagerFactory;

    /// // Fonctions utilitaires :

    public void loadConfigProperties() throws Exception {
        this.configProperties.clear();
        try (FileReader fileReader = new FileReader(DEFAULT_PROPERTIES_FILE_PATH)) {
            this.configProperties.load(fileReader);
        }
    }

    public String getConfigProperty(final String key) {
        if (isEmpty(key)) {
            return null;
        }
        return this.configProperties.getProperty(key);
    }

    public void clearConfigProperties() {
        this.configProperties.clear();
    }

    protected void loadEntityManagerFactory() throws Exception {

        // Récupération
        this.loadConfigProperties();

        // Création de la source de données :
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.getConfigProperty("db.driver"));
        hikariConfig.setJdbcUrl(this.getConfigProperty("db.url"));
        hikariConfig.setUsername(this.getConfigProperty("db.username"));
        hikariConfig.setPassword(this.getConfigProperty("db.password"));
        hikariConfig.setMaximumPoolSize(5);

        // Création de l'instance EntityManagerFactory
        entityManagerFactory = createEntityManagerFactory("testUnit",
                new HikariDataSource(hikariConfig),
                listPackageClasses("org.commons.dao.jpa.beans"));
    }
}
