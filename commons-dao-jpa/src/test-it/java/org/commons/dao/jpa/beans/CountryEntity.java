package org.commons.dao.jpa.beans;

import jakarta.persistence.*;
import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static jakarta.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "country")
public class CountryEntity extends AbstractDAOBean<Long> {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "country")
    private Set<CompanyEntity> companies;

    /// // Constructeur(s)

    public CountryEntity(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public CountryEntity(final String name) {
        this(null, name);
    }

    public CountryEntity() {
        this(null, null);
    }

    /// // Méthodes de la classe AbstractDAOBean :

    @Override
    public int functionnalHashCode() {
        return Objects.hashCode(this.name);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof CountryEntity that)) {
            return false;
        }
        return Objects.equals(this.name, that.name);
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonCountry = new JSONObject();
        jsonCountry.put("id", id);
        jsonCountry.put("name", name);
        return jsonCountry;
    }

    /// //

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public Set<CompanyEntity> getCompanies() {
        return companies;
    }

    public void setCompanies(final Collection<CompanyEntity> companies) {
        this.companies = (companies == null ? new HashSet<>() : new HashSet<>(companies));
        for (CompanyEntity company : this.companies) {
            company.setCountry(this);
        }
    }
}
