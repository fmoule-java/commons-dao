package org.commons.dao.jpa.predicate.operator;

import org.commons.dao.base.DAO;
import org.commons.dao.jpa.AbstractJPATestIT;
import org.commons.dao.jpa.JPADefaultDAO;
import org.commons.dao.jpa.beans.CompanyEntity;
import org.commons.dao.jpa.beans.CountryEntity;
import org.commons.dao.jpa.beans.PersonEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jpa.predicate.general.JPAFieldPredicate.withJPAFieldEqualsTo;
import static org.commons.dao.predicate.operator.OperatorPredicate.and;
import static org.commons.dao.predicate.operator.OperatorPredicate.or;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;

class JPAOperatorPredicateTestIT extends AbstractJPATestIT {
    private DAO<PersonEntity> personDAO;
    private DAO<CompanyEntity> companyDAO;
    private DAO<CountryEntity> countryDAO;

    /// // Méthode(s) statique(s)

    private static CompanyEntity createCompany(final String companyName, final CountryEntity country) {
        final CompanyEntity companyEntity = new CompanyEntity(companyName);
        companyEntity.setCountry(country);
        return companyEntity;
    }

    private static PersonEntity createPerson(final String firstName, final String lastName, CompanyEntity companyEntity) {
        final PersonEntity personEntity = new PersonEntity(firstName, lastName);
        personEntity.setCompany(companyEntity);
        return personEntity;
    }

    /// // Initiolisation

    @BeforeEach
    void setUp() throws Exception {

        this.loadEntityManagerFactory();

        // Initialisation
        this.personDAO = new JPADefaultDAO<>(this.entityManagerFactory, PersonEntity.class);
        this.companyDAO = new JPADefaultDAO<>(this.entityManagerFactory, CompanyEntity.class);
        this.countryDAO = new JPADefaultDAO<>(this.entityManagerFactory, CountryEntity.class);

        // Initialisation
        this.personDAO.deleteAll();
        this.companyDAO.deleteAll();
        this.countryDAO.deleteAll();
    }

    @AfterEach
    void tearDown() {
        closeQuietly(this.personDAO);
        closeQuietly(this.entityManagerFactory);
    }

    /// // Tests fonctionnels

    @Test
    public void shouldSelectWithJPAAndCondition() throws Exception {

        // Bouchonnage de la base de données :
        this.personDAO.save(new PersonEntity("frederic", "moule"));
        this.personDAO.save(new PersonEntity("gael", "lebreton"));
        this.personDAO.save(new PersonEntity("gael", "dupont"));
        this.personDAO.save(new PersonEntity("cyril", "courtonne"));

        // Sélectionne
        final List<PersonEntity> foundPersons = this.personDAO.select(and(withJPAFieldEqualsTo("firstName", "gael"),
                withJPAFieldEqualsTo("lastName", "lebreton")));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons).hasSize(1);
        final PersonEntity foundPerson = foundPersons.getFirst();
        assertThat(foundPerson.getId()).isGreaterThan(0L);
        assertThat(foundPerson.getFirstName()).isEqualTo("gael");
        assertThat(foundPerson.getLastName()).isEqualTo("lebreton");
    }

    @Test
    public void shouldSelectWithComplexeJPAAndCondition() throws Exception {

        // Bouchonnage des pays
        final CountryEntity usaCountry = this.countryDAO.save(new CountryEntity("USA"));
        final CountryEntity franceCountry = this.countryDAO.save(new CountryEntity("France"));

        // Bouchonnage des entreprises :
        final CompanyEntity googleCompany = this.companyDAO.save(createCompany("Google", usaCountry));
        final CompanyEntity atosCompany = this.companyDAO.save(createCompany("Atos", franceCountry));
        final CompanyEntity freeCompany = this.companyDAO.save(createCompany("Free", franceCountry));

        // Bouchonnage de la base de données :
        this.personDAO.save(createPerson("frederic", "moule", atosCompany));
        this.personDAO.save(createPerson("gael", "lebreton", atosCompany));
        this.personDAO.save(createPerson("gael", "dupont", googleCompany));
        this.personDAO.save(createPerson("cyril", "courtonne", freeCompany));

        // Sélectionne
        List<PersonEntity> foundPersons = this.personDAO.select(and(withJPAFieldEqualsTo("firstName", "gael"),
                withJPAFieldEqualsTo("company.country.name", "France")));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons).hasSize(1);
        final PersonEntity foundPerson = foundPersons.getFirst();
        assertThat(foundPerson.getId()).isGreaterThan(0L);
        assertThat(foundPerson.getFirstName()).isEqualTo("gael");
        assertThat(foundPerson.getLastName()).isEqualTo("lebreton");

        // 2ième cas
        foundPersons = this.personDAO.select(and(withJPAFieldEqualsTo("firstName", "cyril"),
                withJPAFieldEqualsTo("company.country.name", "USA")));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isEmpty();

    }

    @Test
    public void shouldSelectWithJPAOrCondition() throws Exception {

        // Bouchonnage de la base de données :
        this.personDAO.save(new PersonEntity("frederic", "moule"));
        this.personDAO.save(new PersonEntity("gael", "lebreton"));
        this.personDAO.save(new PersonEntity("gael", "dupont"));
        this.personDAO.save(new PersonEntity("cyril", "courtonne"));

        // Sélectionne
        final List<PersonEntity> foundPersons = this.personDAO.select(or(withJPAFieldEqualsTo("firstName", "gael"),
                withJPAFieldEqualsTo("firstName", "frederic")));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons).hasSize(3);

        // Vérif
        PersonEntity foundPerson = foundPersons.getFirst();
        assertThat(foundPerson.getId()).isGreaterThan(0L);
        assertThat(foundPerson.getFirstName()).isEqualTo("frederic");
        assertThat(foundPerson.getLastName()).isEqualTo("moule");
        foundPerson = foundPersons.get(1);
        assertThat(foundPerson.getId()).isGreaterThan(0L);
        assertThat(foundPerson.getFirstName()).isEqualTo("gael");
        assertThat(foundPerson.getLastName()).isEqualTo("lebreton");
        foundPerson = foundPersons.get(2);
        assertThat(foundPerson.getId()).isGreaterThan(0L);
        assertThat(foundPerson.getFirstName()).isEqualTo("gael");
        assertThat(foundPerson.getLastName()).isEqualTo("dupont");
    }

}