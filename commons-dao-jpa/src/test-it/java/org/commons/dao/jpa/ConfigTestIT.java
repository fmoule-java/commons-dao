package org.commons.dao.jpa;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigTestIT extends AbstractJPATestIT {

    /// // Initialisation

    @BeforeEach
    void setUp() throws Exception {
        this.loadConfigProperties();
    }

    @AfterEach
    void tearDown() {
        this.clearConfigProperties();
    }

    /// // Tests unitaires :

    @Test
    public void shouldLoadProperties() {
        String configPropertyValue = this.getConfigProperty("db.url");
        assertThat(configPropertyValue).isNotNull();
        assertThat(configPropertyValue).isNotEmpty();
        configPropertyValue = this.getConfigProperty("not.existing.key");
        assertThat(configPropertyValue).isNull();
        configPropertyValue = this.getConfigProperty("");
        assertThat(configPropertyValue).isNull();
        configPropertyValue = this.getConfigProperty(null);
        assertThat(configPropertyValue).isNull();
    }
}
