package org.commons.dao.jpa.beans;

import jakarta.persistence.*;
import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Objects;

import static jakarta.persistence.GenerationType.IDENTITY;

@SuppressWarnings("MethodDoesntCallSuperMethod")
@Entity
@Table(name = "person")
public class PersonEntity extends AbstractDAOBean<Long> {

    @Id
    @GeneratedValue (strategy = IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private CompanyEntity company;

    /// // Constructeur(s)

    public PersonEntity(final Long id, final String firstName, final String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public PersonEntity(final String firstName, final String lastName) {
        this(null, firstName, lastName);
    }

    public PersonEntity() {
        this(null, null, null);
    }

    /// // Méthode(s) de la classe AbstractDAOBean :

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof PersonEntity personEntity)) {
            return false;
        }
        boolean areEquals = Objects.equals(firstName, personEntity.firstName);
        areEquals = areEquals && Objects.equals(lastName, personEntity.lastName);
        return areEquals;
    }

    @Override
    public int functionnalHashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonPerson = new JSONObject();
        jsonPerson.put("name", this.lastName);
        jsonPerson.put("fisrtName", this.firstName);
        if (this.company != null) {
            jsonPerson.put("company", this.company.toJSON());
        }
        return jsonPerson;
    }

    @Override
    public PersonEntity clone() throws CloneNotSupportedException {
        PersonEntity clonedPersonEntity = new PersonEntity(id, firstName, lastName);
        if (company != null) {
            clonedPersonEntity.setCompany(company.clone());
        }
        return clonedPersonEntity;
    }

    /// // Getters & Setters :

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setCompany(final CompanyEntity company) {
        this.company = company;
    }

    public CompanyEntity getCompany() {
        return company;
    }
}
