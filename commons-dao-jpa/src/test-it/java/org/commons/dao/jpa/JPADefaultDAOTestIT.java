package org.commons.dao.jpa;

import org.commons.dao.jpa.beans.CompanyEntity;
import org.commons.dao.jpa.beans.PersonEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.base.DAO.closeDAO;
import static org.commons.dao.jpa.predicate.general.JPAFieldPredicate.withJPAFieldEqualsTo;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;

public class JPADefaultDAOTestIT extends AbstractJPATestIT {
    private JPADefaultDAO<PersonEntity> personDAO;
    private JPADefaultDAO<CompanyEntity> companyDAO;

    /// // Initialisation

    @BeforeEach
    void setUp() throws Exception {

        loadEntityManagerFactory();
        personDAO = new JPADefaultDAO<>(entityManagerFactory, PersonEntity.class);
        companyDAO = new JPADefaultDAO<>(entityManagerFactory, CompanyEntity.class);

        // Nettoyage de la base
        personDAO.deleteAll();
    }

    @AfterEach
    void tearDown() throws IOException {

        // Fermeture
        this.clearConfigProperties();

        // Fermeture des DAO's
        closeDAO(companyDAO);
        closeDAO(personDAO);
        closeQuietly(entityManagerFactory);
    }

    /// // Tests fonctionnels

    @Test
    public void shouldGetEntityManagerFactory() {
        assertThat(this.entityManagerFactory).isNotNull();
    }

    @Test
    public void shouldSelect() throws Exception {
        // Création
        final PersonEntity personEntity = new PersonEntity();
        personEntity.setFirstName("Frédéric");
        personEntity.setLastName("Moulé");
        final PersonEntity savedPersonEntity = personDAO.save(personEntity);
        assertThat(savedPersonEntity).isNotNull();
        assertThat(savedPersonEntity.getId()).isNotNull();

        // Récupération de la liste
        final List<PersonEntity> personEntities = personDAO.selectAll();
        assertThat(personEntities).isNotNull();
        assertThat(personEntities.size()).isEqualTo(1);
    }

    @Test
    public void shouldGetCopy() throws Exception {

        // Création
        final PersonEntity personEntity = new PersonEntity();
        personEntity.setFirstName("Frédéric");
        personEntity.setLastName("Moulé");
        final PersonEntity savedPersonEntity = personDAO.save(personEntity);
        assertThat(savedPersonEntity).isNotNull();

        // On modifie
        savedPersonEntity.setFirstName("fred");

        // On vérifie la couche de persistence
        final Optional<PersonEntity> foundPersonEntityOpt = personDAO
                .selectFirst(withJPAFieldEqualsTo("lastName", "Moulé"));
        assertThat(foundPersonEntityOpt).isNotNull();
        assertThat(foundPersonEntityOpt.isPresent()).isTrue();
        final PersonEntity foundPerson = foundPersonEntityOpt.get();
        assertThat(foundPerson).isNotNull();
        assertThat(foundPerson.getFirstName()).isEqualTo("Frédéric");
    }

    @Test
    public void shouldSaveACollection() throws Exception {

        // Sauvegarde
        final List<PersonEntity> personEntities = new ArrayList<>();
        personEntities.add(new PersonEntity("frederic", "moule"));
        personEntities.add(new PersonEntity("gael", "lebreton"));
        personEntities.add(new PersonEntity("laurent", "pechereau"));
        personDAO.save(personEntities);

        // Vérification
        assertThat(personDAO.selectCount()).isEqualTo(3);
    }

    @Test
    public void shouldSaveWithCompany() throws Exception {

        // Création
        final PersonEntity personEntity = new PersonEntity();
        personEntity.setFirstName("Frédéric");
        personEntity.setLastName("Moulé");
        personEntity.setCompany(new CompanyEntity("fredCompany"));
        personDAO.save(personEntity);

        // Récupération de la base
        assertThat(personDAO.selectCount()).isEqualTo(1);
        final Optional<PersonEntity> foundPersonOpt = personDAO.selectFirst(Objects::nonNull);
        assertThat(foundPersonOpt).isNotNull();
        assertThat(foundPersonOpt).isPresent();
        final PersonEntity foundPerson = foundPersonOpt.get();
        assertThat(foundPerson.getFirstName()).isEqualTo("Frédéric");
        assertThat(foundPerson.getLastName()).isEqualTo("Moulé");
        final CompanyEntity company = foundPerson.getCompany();
        assertThat(company).isNotNull();
        assertThat(company.getId()).isGreaterThan(0L);
        assertThat(company.getName()).isEqualTo("fredCompany");
    }

}