package org.commons.dao.jpa.beans;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CompanyEntityTest {

    /// // Tests unitaires

    @Test
    public void shouldBeEquals() {
        CompanyEntity company = new CompanyEntity(12L, "comp");
        assertThat(company).isEqualTo(new CompanyEntity(12L, "comp"));
        assertThat(company.hashCode()).isEqualTo(new CompanyEntity(12L, "comp").hashCode());
        assertThat(company).isEqualTo(new CompanyEntity(12L, "otherCompanyName"));
        assertThat(company.hashCode()).isEqualTo(new CompanyEntity(12L, "otherCompanyName").hashCode());
        assertThat(company).isNotEqualTo(new CompanyEntity(144L, "comp"));
        assertThat(company).isNotEqualTo(null);
        company = new CompanyEntity(null, "companyName");
        assertThat(company).isEqualTo(new CompanyEntity(null, "companyName"));
        assertThat(company.hashCode()).isEqualTo(new CompanyEntity(null, "companyName").hashCode());
        assertThat(company).isNotEqualTo(new CompanyEntity(145L, "companyName"));
    }

}