package org.commons.dao.jpa.predicate.general;

import org.commons.dao.base.DAO;
import org.commons.dao.jpa.AbstractJPATestIT;
import org.commons.dao.jpa.JPADefaultDAO;
import org.commons.dao.jpa.beans.CompanyEntity;
import org.commons.dao.jpa.beans.CountryEntity;
import org.commons.dao.jpa.beans.PersonEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jpa.predicate.general.JPAFieldPredicate.withJPAFieldEqualsTo;
import static org.commons.dao.utils.io.IOUtils.closeQuietly;

class JPAFieldPredicateTestIT extends AbstractJPATestIT {
    private DAO<PersonEntity> personDAO;
    private DAO<CompanyEntity> companyDAO;
    private DAO<CountryEntity> countryDAO;

    /// // Méthodes statiques :

    private static PersonEntity createPersonEntity(final String firstName,
                                                   final String lastName,
                                                   final CompanyEntity company) {
        final PersonEntity personEntity = new PersonEntity(firstName, lastName);
        personEntity.setCompany(company);
        return personEntity;
    }

    private static CompanyEntity createCompany(final String companyName, CountryEntity countryEntity) {
        final CompanyEntity companyEntity = new CompanyEntity(companyName);
        companyEntity.setCountry(countryEntity);
        return companyEntity;
    }

    /// Initialisation //

    @BeforeEach
    void setUp() throws Exception {

        // Chargement
        this.loadEntityManagerFactory();

        // Initialisation
        personDAO = new JPADefaultDAO<>(this.entityManagerFactory, PersonEntity.class);
        companyDAO = new JPADefaultDAO<>(this.entityManagerFactory, CompanyEntity.class);
        countryDAO = new JPADefaultDAO<>(this.entityManagerFactory, CountryEntity.class);

        // Nettoyage
        this.personDAO.deleteAll();
        this.companyDAO.deleteAll();
    }

    @AfterEach
    void tearDown() {
        closeQuietly(this.personDAO);
        closeQuietly(this.companyDAO);
        closeQuietly(this.entityManagerFactory);
    }

    /// // Tests fonctionnels :

    @Test
    public void shouldSelectFirstFromPredicate() throws Exception {

        // Bouchonnage de la base de données :
        personDAO.save(new PersonEntity("frederic", "moule"));
        personDAO.save(new PersonEntity("gael", "lebreton"));
        personDAO.save(new PersonEntity("laurent", "pechereau"));
        assertThat(this.personDAO.selectCount()).isEqualTo(3L);

        final Optional<PersonEntity> foundPersonOpt = this.personDAO
                .selectFirst(withJPAFieldEqualsTo("firstName", "gael"));
        assertThat(foundPersonOpt).isNotNull();
        assertThat(foundPersonOpt).isPresent();
        final PersonEntity foudPersonEntity = foundPersonOpt.get();
        assertThat(foudPersonEntity).isNotNull();
        assertThat(foudPersonEntity.getFirstName()).isEqualTo("gael");
        assertThat(foudPersonEntity.getLastName()).isEqualTo("lebreton");
        assertThat(foudPersonEntity.getId()).isNotNull();
        assertThat(foudPersonEntity.getId()).isGreaterThan(0L);
    }

    @Test
    public void shouldNotFindFromPredicate() throws Exception {

        // Bouchonnage de la base de données :
        personDAO.save(new PersonEntity("frederic", "moule"));
        personDAO.save(new PersonEntity("gael", "lebreton"));
        personDAO.save(new PersonEntity("laurent", "pechereau"));
        assertThat(this.personDAO.selectCount()).isEqualTo(3L);

        // Recherche
        final Optional<PersonEntity> foundPersonOpt = this.personDAO.selectFirst(withJPAFieldEqualsTo("firstName", "alexandre"));
        assertThat(foundPersonOpt).isNotNull();
        assertThat(foundPersonOpt).isEmpty();
    }

    @Test
    public void shouldSelectListFromPredicate() throws Exception {

        // Bouchonnage de la base de données :
        personDAO.save(new PersonEntity("frederic", "moule"));
        personDAO.save(new PersonEntity("gael", "lebreton"));
        personDAO.save(new PersonEntity("laurent", "pechereau"));
        assertThat(this.personDAO.selectCount()).isEqualTo(3L);

        final List<PersonEntity> foundPersons = this.personDAO.select(withJPAFieldEqualsTo("firstName", "frederic"));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons.size()).isEqualTo(1);

        // Vérification
        final PersonEntity foundPerson = foundPersons.getFirst();
        assertThat(foundPerson).isNotNull();
        assertThat(foundPerson.getFirstName()).isEqualTo("frederic");
        assertThat(foundPerson.getLastName()).isEqualTo("moule");
        assertThat(foundPerson.getId()).isGreaterThan(0L);
    }

    @Test
    public void shouldTest() {
        final Predicate<PersonEntity> predicate = withJPAFieldEqualsTo("company.name", "fredCompany");
        assertThat(predicate.test(createPersonEntity("frederic", "moule", new CompanyEntity("fredCompany"))))
                .isTrue();
        assertThat(predicate.test(createPersonEntity("gael", "lebreton", new CompanyEntity("gaelCompany"))))
                .isFalse();
        assertThat(predicate.test(null)).isFalse();
    }

    @Test
    public void shouldSelectFromCompositeField() throws Exception {

        // Création des pays
        final CountryEntity franceCountry = this.countryDAO.save(new CountryEntity("france"));
        final CountryEntity allemagneCountry = this.countryDAO.save(new CountryEntity("allemagne"));

        // Création de la compagnie
        final CompanyEntity savedFredCompany = this.companyDAO.save(createCompany("fredCompany", franceCountry));
        final CompanyEntity savedGaelCompany = this.companyDAO.save(createCompany("gaelCompany", allemagneCountry));

        // Création des utilisateurs :
        this.personDAO.save(createPersonEntity("frederic", "moule", savedFredCompany));
        this.personDAO.save(createPersonEntity("laurent", "pechereau", savedFredCompany));
        this.personDAO.save(createPersonEntity("gael", "lebreton", savedGaelCompany));

        // Sélection 1er nivean
        List<PersonEntity> foundPersons = this.personDAO.select(withJPAFieldEqualsTo("company.name", "fredCompany"));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons.size()).isEqualTo(2);

        // 2ième niveau :
        foundPersons = this.personDAO.select(withJPAFieldEqualsTo("company.country.name", "allemagne"));
        assertThat(foundPersons).isNotNull();
        assertThat(foundPersons).isNotEmpty();
        assertThat(foundPersons.size()).isEqualTo(1);
    }


}