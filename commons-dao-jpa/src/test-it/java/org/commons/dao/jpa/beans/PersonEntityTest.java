package org.commons.dao.jpa.beans;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonEntityTest {

    /// // Tests unitaires :

    @Test
    public void shouldCopyPersonEntity() throws CloneNotSupportedException {
        final PersonEntity personEntity = new PersonEntity("frédéric", "moulé");
        final PersonEntity clonedPersonEntity = personEntity.clone();
        assertThat(clonedPersonEntity.getFirstName()).isEqualTo("frédéric");
        assertThat(clonedPersonEntity.getLastName()).isEqualTo("moulé");
        assertThat(personEntity != clonedPersonEntity).isTrue();
    }

    @Test
    public void shouldCopyWihCompanyAndId() throws CloneNotSupportedException {
        final PersonEntity personEntity = new PersonEntity(14L, "frédéric", "moulé");
        personEntity.setCompany(new CompanyEntity(4L, "fredCompany"));
        final PersonEntity clonedPersonEntity = personEntity.clone();
        assertThat(clonedPersonEntity.getFirstName()).isEqualTo("frédéric");
        assertThat(clonedPersonEntity.getLastName()).isEqualTo("moulé");
        assertThat(personEntity != clonedPersonEntity).isTrue();
        CompanyEntity clonedCompany = clonedPersonEntity.getCompany();
        assertThat(clonedCompany).isNotNull();
        assertThat(clonedCompany.getId()).isEqualTo(4L);
        assertThat(clonedCompany.getName()).isEqualTo("fredCompany");
        assertThat(clonedCompany != personEntity.getCompany()).isTrue();
    }

}