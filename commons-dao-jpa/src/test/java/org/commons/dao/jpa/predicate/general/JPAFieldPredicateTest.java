package org.commons.dao.jpa.predicate.general;

import org.commons.dao.jpa.bean.TestDAOBean;
import org.commons.dao.jpa.predicate.JPAPredicate;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.commons.dao.jpa.predicate.general.JPAFieldPredicate.withJPAFieldEqualsTo;

class JPAFieldPredicateTest {

    /// // Tests unitaires :

    @Test
    public void shouldComposeOr() {
        Predicate<TestDAOBean> predicate = withJPAFieldEqualsTo("lastName", "moulé");
        predicate = predicate.or(withJPAFieldEqualsTo("firstName", "frédéric"));
        assertThat(predicate).isNotNull();
        assertThat(predicate instanceof JPAPredicate<TestDAOBean>).isTrue();
    }
}