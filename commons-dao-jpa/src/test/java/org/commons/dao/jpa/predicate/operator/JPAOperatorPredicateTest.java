package org.commons.dao.jpa.predicate.operator;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class JPAOperatorPredicateTest {

    @Test
    public void shouldConvertIntoArray() {
        final List<String> toto = List.of("toto", "titu");
        final String[] strArrays = toto.toArray(String[]::new);
        assertThat(strArrays.length).isEqualTo(2);
        assertThat(strArrays).containsExactly("toto", "titu");
    }

}