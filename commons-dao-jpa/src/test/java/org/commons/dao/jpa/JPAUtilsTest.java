package org.commons.dao.jpa;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JPAUtilsTest {

    /// // Tests unitaires

    @Test
    public void shouldGetClassName() {
        assertThat(HibernatePersistenceProvider.class.getName())
                .isEqualTo("org.hibernate.jpa.HibernatePersistenceProvider");
    }


}