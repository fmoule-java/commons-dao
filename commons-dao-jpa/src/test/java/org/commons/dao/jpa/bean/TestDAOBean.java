package org.commons.dao.jpa.bean;

import org.commons.dao.bean.AbstractDAOBean;
import org.json.JSONObject;

import java.util.Objects;

public class TestDAOBean extends AbstractDAOBean<Long> {
    private Long id;
    private String name;

    /// // Méthodes

    @Override
    public int functionnalHashCode() {
        return Objects.hashCode(this.name);
    }

    @Override
    public boolean isFunctionnallyEquals(final AbstractDAOBean<?> obj) {
        if (!(obj instanceof TestDAOBean testDAOBean)) {
            return false;
        }
        return Objects.equals(name, testDAOBean.name);
    }

    @Override
    public JSONObject toJSON() {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("name", name);
        return jsonObject;
    }

    /// // Getters

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {

    }
}
